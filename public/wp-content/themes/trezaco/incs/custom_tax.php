<?php

add_action( 'init', function(){

    // register_taxonomy(
    //     'categoria_evento',
    //     'eventos',
    //     array(
    //         'label' => __( 'Categorias de eventos' ),
    //         'rewrite' => false,
    //         'hierarchical' => true
    //     )
    // );

    register_taxonomy(
        'categoria_produto',
        'produtos',
        array(
            'label' => __( 'Categorias de Produtos' ),
            'rewrite' => false,
            'hierarchical' => true
        )
    );

    register_taxonomy(
        'categoria_norma',
        'normas',
        array(
            'label' => __( 'Categorias de Normas' ),
            'rewrite' => false,
            'hierarchical' => true
        )
    );

    register_taxonomy(
        'categoria_blog',
        'blog',
        array(
            'label' => __( 'Categorias de Blog' ),
            'rewrite' => false,
            'hierarchical' => true
        )
    );
});

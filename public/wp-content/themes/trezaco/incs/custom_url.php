<?php
add_action( 'init', function(){

    add_rewrite_tag('%categoriablog%', '([0-9a-zA-Z\-]*)');
    add_rewrite_tag('%categoriaprodutos%', '([0-9a-zA-Z\-]*)');
    add_rewrite_tag('%subcategoriaprodutos%', '([0-9a-zA-Z\-]*)');
    add_rewrite_tag('%pesquisablog%', '([0-9a-zA-Z\-]*)');

    add_rewrite_rule('^blog/pesquisa/([0-9a-zA-Z\-]*)?','index.php?pagename=blog&pesquisablog=$matches[1]', 'top');
    add_rewrite_rule('^blog/categoria/([0-9a-zA-Z\-]*)?','index.php?pagename=blog&categoriablog=$matches[1]', 'top');

    add_rewrite_rule('^produtos/categoria/([0-9a-zA-Z\-]*)/([0-9a-zA-Z\-]*)?','index.php?pagename=produtos&categoriaprodutos=$matches[1]&subcategoriaprodutos=$matches[2]', 'top');
    add_rewrite_rule('^produtos/categoria/([0-9a-zA-Z\-]*)?','index.php?pagename=produtos&categoriaprodutos=$matches[1]', 'top');

    flush_rewrite_rules();

});

<?php
//******* LINKS PARA SCRIPTS CSS E JS */

/******* Scripts padrões *******/
include __DIR__."/incs/links_base.php"; 

/******* Links de plugins *******/
include __DIR__."/incs/links_plugins.php"; 

session_start();
/******************************/


//****** CUSTOM */

/******* Custom config *******/
include __DIR__."/incs/custom_config.php";

/******* Custom image size *******/
include __DIR__."/incs/custom_image_sizes.php";

/******* Custom URL *******/
include __DIR__."/incs/custom_url.php";

//****** INFORMAÇÕES GLOBAIS

//include __DIR__."/incs/custom_info.php";


/******************************/

//******* ACF OPTIONS */

/******* Configurações para o acf options ********/
include __DIR__."/incs/acf_options.php";

/******* Custom post types *******/
include __DIR__."/incs/custom_type.php";

/******* Custom taxonomy *******/
include __DIR__."/incs/custom_tax.php";

function adicionarProduto() {
    $produtos = $_SESSION["carrinho"];
    if($produtos == null){
        $produtos = array();
    }
    $referencia = $_POST['referencia'];
    $nome = $_POST['nome'];
    $imagemUrl = $_POST['imagem_url'];
    $quantidade = $_POST['quantidade'];
    $titulosMedidas = $_POST['titulos_medidas'];
    $valoresMedidas = $_POST['medidas_valores'];
    $idProduto = $_POST['produto_id'];

    $produto = new stdClass();
    $produto->imagem_url = $imagemUrl;
    $produto->nome = $nome; 
    $produto->referencia = $referencia; 
    $produto->quantidade = $quantidade;
    $produto->medidas = new stdClass;

    $titulosMedidasArray = explode('-', $titulosMedidas);
    $valoresMedidasArray = explode('|', $valoresMedidas);
    $contadorMedidas = count($titulosMedidasArray);
    for($i = 0; $i < $contadorMedidas; $i++){
        $tituloMedidaArray = $titulosMedidasArray[$i];
        $produto->medidas->$tituloMedidaArray = $valoresMedidasArray[$i];
    }

    array_push($produtos, $produto);
    $_SESSION["carrinho"] = $produtos;

    echo count($_SESSION["carrinho"]) . '|';
    die();
}
add_action('wp_ajax_adicionarProduto', 'adicionarProduto');
add_action('wp_ajax_nopriv_adicionarProduto', 'adicionarProduto');

function atualizarCarrinho() {

    $jsonData = stripslashes($_POST['carrinho']);

    $carrinho = json_decode($jsonData, true);

    $_SESSION["carrinho"] = $carrinho['itens'];
    echo count($_SESSION["carrinho"]);
    die();
}
add_action('wp_ajax_atualizarCarrinho', 'atualizarCarrinho');
add_action('wp_ajax_nopriv_atualizarCarrinho', 'atualizarCarrinho');


function be_ajax_load_more() {

    $postsCarregado = 0;
    $args = array(
        'numberposts'      => 8,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'posts_per_page' => 8,
        'paged' => $_POST['page'],
        'post_type'  => 'blog'
    );
    $postsCarregado = get_posts($args);

    $containerResponse = array( );
    $countPosts = count($postsCarregado);
    if($countPosts == 0){
        echo 'false';
        die();
    }else{
        for($i = 0; $i < $countPosts; $i++){
            $titulo = wp_strip_all_tags(get_field('titulo',$postsCarregado[$i]));
            if(strlen($titulo) > 40){
                $titulo = substr($titulo, 0, 37)."..."; 
            }else{
                $titulo = $titulo;  
            }
            $resumo = wp_strip_all_tags(get_field('texto',$postsCarregado[$i]));
            if(strlen($resumo) > 130){
                $resumo = substr($resumo, 0, 130) . "..."; 
            }else{
                $resumo = $resumo;
            }

            $link = $postsCarregado[$i]->post_name;

            $imagem = get_field('imagem_destaque', $postsCarregado[$i])['url'];

            array_push($containerResponse, array("titulo" => $titulo, "resumo" => $resumo, "link" => $link, "imagem" => $imagem));
        }

        $postsJson = json_decode($postsCarregado, true);

        echo json_encode($containerResponse);
        die();
    }
}
add_action( 'wp_ajax_be_ajax_load_more', 'be_ajax_load_more' );
add_action( 'wp_ajax_nopriv_be_ajax_load_more', 'be_ajax_load_more' );

function atualizarCarrinhoLista() {
    if(!empty($_SESSION["carrinho"])){
        $carrinhoProdutosEscolhidos = $_SESSION["carrinho"];
        $carrinhoQuantidadeProdutosEscolhidos = count($carrinhoProdutosEscolhidos);
        $carrinhoProdutosJson = json_encode($carrinhoProdutosEscolhidos);
        $carrinhoQuantidadeProdutosEscolhidosTotal = count($carrinhoProdutosEscolhidos);
    }else{
        $carrinhoProdutosEscolhidos = '';
        $carrinhoQuantidadeProdutosEscolhidos = 0;
        $carrinhoProdutosJson = '';
    }

    $containerResponseCarrinho = array( );

    if($carrinhoQuantidadeProdutosEscolhidos == 0){
        echo 'false';
        die();
    }else{
        $carrinhoQuantidadeProdutosEscolhidos = $carrinhoQuantidadeProdutosEscolhidos < 4 ? $carrinhoQuantidadeProdutosEscolhidos : 3;
        for($i = 0; $i < $carrinhoQuantidadeProdutosEscolhidos; $i++){
            $produtoCarrinho = (object) $carrinhoProdutosEscolhidos[$i];
            $nomeCarrinho = $produtoCarrinho->nome;
            $referenciaCarrinho = $produtoCarrinho->referencia;
            $quantidadeCarrinho = $produtoCarrinho->quantidade;
            $quantidadeTotalCarrinho = $carrinhoQuantidadeProdutosEscolhidosTotal;
            $medidasArray = json_decode(json_encode($produtoCarrinho->medidas), true);
            $medidas = '';
            foreach($medidasArray as $key=>$value){
                $medidas = $medidas . $key . " de " . $value . "\n";
            }
            array_push($containerResponseCarrinho, array("nome" => $nomeCarrinho, "referencia" => $referenciaCarrinho, "quantidade" => $quantidadeCarrinho, "medidas" => $medidas, "total" => $quantidadeTotalCarrinho));
        }

        $produtosCarrinhosJson = json_decode($carrinhoProdutosEscolhidos, true);

        echo json_encode($containerResponseCarrinho);
        die();
    }
}
add_action('wp_ajax_atualizarCarrinhoLista', 'atualizarCarrinhoLista');
add_action('wp_ajax_nopriv_atualizarCarrinhoLista', 'atualizarCarrinhoLista');

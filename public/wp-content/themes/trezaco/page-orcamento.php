<?php
    wp_enqueue_style('css_orcamento', get_stylesheet_directory_uri().'/src/css/orcamento.min.css', array(), null, false);
    get_header();

    if(!empty($_SESSION["carrinho"])){
        $produtosEscolhidos = $_SESSION["carrinho"];
        $quantidadeProdutosEscolhidos = count($produtosEscolhidos);
        $produtosJson = json_encode($produtosEscolhidos);
    }else{
        $produtosEscolhidos = '';
        $quantidadeProdutosEscolhidos = 0;
        $produtosJson = '';
    }
    
?>
<div class="container-breadcrumb">
    <div class="container-padrao">
        <img class="home" src="<?php echo get_stylesheet_directory_uri()?>/img/home-solid.svg">
        <img class="arrow" src="<?php echo get_stylesheet_directory_uri()?>/img/orange-next-arrow.svg">
        <a href="/orcamento">Orçamento</a>
    </div>
</div>
<div class="container-produtos-pai">
    <div class="container-padrao">
        <h1>Produto(s)</h1>
        <div class="container-produtos">
            <?php 
                if($quantidadeProdutosEscolhidos != 0){
                    for($i = 0; $i < $quantidadeProdutosEscolhidos; $i++){
                        $produto = (object) $produtosEscolhidos[$i];
                        $nome = $produto->nome;
                        $referencia = $produto->referencia;
                        $imagem = $produto->imagem_url;
                        $quantidade = $produto->quantidade;
                        $medidas = json_decode(json_encode($produto->medidas), true);
            ?>
                    <div class="container-produto linha-produto" id="linha-produto-<?=$i?>">
                        <div class="container-info-pai">
                            <div class="container-imagem">
                                <img src="<?=$imagem?>">
                            </div>
                            <div class="container-info">
                                <h1><?=$nome?></h1>
                                <p>
                                    <?php
                                        foreach($medidas as $key=>$value){
                                            echo "$key de $value<br />";
                                        }
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="container-quantidade-pai">
                            <div class="container-quantidade">
                                <p>Qtd</p>
                                <div class="quantidade-produto">
                                    <button onclick="aumentarDiminuirQuantidadeProdutoA('<?= $i ?>', '-')">
                                        -
                                    </button>
                                    <input type="number" class="quantidade-produto-input" id="quantidade-produto-<?= $i ?>" readonly="readonly" value="<?=$quantidade?>">
                                    <button onclick="aumentarDiminuirQuantidadeProdutoA('<?= $i ?>', '+')">
                                        +
                                    </button>
                                </div>
                            </div>
                            <div class="container-remove">
                                <div class="container-remove-image" onclick="cancelarProduto('<?= $i ?>')">
                                    <img src="<?=get_stylesheet_directory_uri()?>/img/lixeira.svg">
                                </div>
                            </div>
                        </div>
                    </div>
            <?php } }else{ ?>
                <h1 id="nao-tem-produto">Nenhum produto foi adicionado ao seu carrinho</h1>
            <?php } ?>
        </div>
    </div>
</div>
<div class="container-formulario-veja">
    <div class="container-background">
        <img src="<?=get_field('bloco_formulario')['imagem_background']['url']?>">
    </div>
    <div class="container-formulario-pai">
        <div class="container-padrao">
            <div class="container-formulario">
                <h1><?=get_field('bloco_formulario')['titulo']?></h1>
                <form id="form" class="form" data-hc-form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
                    <p>Nome completo</p>
                    <input type="text" name="name" placeholder="Digite aqui o seu nome" required hc-mail-message>
                    <p>Seu e-mail</p>
                    <input type="email" name="email" placeholder="Digite aqui o seu e-mail de contato" required hc-mail-message>
                    <p>Telefone</p>
                    <input type="text" name="telefone" placeholder="Digite aqui o seu telefone" required hc-mail-message>
                    <p>Mensagem</p>
                    <textarea name="mensagem" placeholder="Digite sua mensagem" required hc-mail-message></textarea>
                    <div class="container-retirada">
                        <input type="checkbox" id="retirada-local" name="retirada-local">
                        <label for="retirada-local">Marque está opção caso a retirada seja no local</label>
                    </div>
                    <textarea hidden name="itens" id="txt_itens" hc-mail-message></textarea>
                    <div class="container-botoes">
                        <a href="/produtos"><img src="<?=get_stylesheet_directory_uri()?>/img/arrow-left.svg" >Escolher mais produtos </a>
                        <button type="submit">Enviar Orçamento</button>
                        <p onclick="enviarViaWhatsapp('<?=get_field('bloco_formulario')['numero_whats']?>')" id="whats"><img src="<?=get_stylesheet_directory_uri()?>/img/whatsapp-brands.svg">Enviar Orçamento Via WhatsApp</p>
                    </div>
                    <div data-hc-feedback class="container-formulario-response orcamento" id="container-formulario-response">
                        <div class="container-response">
                            <div class="container-conteudo">
                                <div class="container-cancelar" onclick="modalOrcamentoController()">
                                    <img src="<?=get_stylesheet_directory_uri()?>/img/close.svg">
                                </div>
                                <img src="<?=get_stylesheet_directory_uri()?>/img/success.svg">
                                <h1>Orçamento enviado com sucesso</h1>
                                <p>Aguarde que logo entraremos em contato.</p>
                                <a href="/"><img src="<?=get_stylesheet_directory_uri()?>/img/arrow-left.svg" >Voltar para página inicial</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-veja-tambem">
        <div class="container-padrao">
            <h1 class="titulo"><?=get_field('bloco_veja_tambem')['titulo']?></h1>
            <div class="container-produtos">
                <?php 
                    $argsCategorias = array(
                        'post_type'  => 'produtos',
                        'taxonomy' => 'categoria_produto',
                        'parent' => '0',
                        'hide_empty' => false,
                        'orderby'	=> 'meta_value',
                        'order' => "ASC",
                        'meta_key' => 'ordem_categoria'
                    );  
                    $produtosCategorias = get_terms($argsCategorias);

                    $contadorCategorias = count($produtosCategorias);
                    for($i = 0; $i < $contadorCategorias; $i++){
                        $categoriaPai = $produtosCategorias[$i];
                        if($categoriaPai->slug == 'conexoes' || $categoriaPai->slug == 'valvulas'){
                        }else{
                ?>
                    <a href="/produtos/categoria/<?=$categoriaPai->slug?>">
                        <div class="container-produto">
                            <div class="container-imagem">
                                <img src="<?=!empty(get_field('imagem_em_destaque', $categoriaPai)['url']) ? get_field('imagem_em_destaque', $categoriaPai)['url'] : get_stylesheet_directory_uri().'/img/iron-bar.svg'?>">
                            </div>
                            <div class="container-conteudo">
                                <h1><?=$categoriaPai->name?></h1>
                            </div>
                        </div>
                    </a>
                <?php } }?>
            </div>
        </div>
    </div>
</div>
<script>
    function modalOrcamentoController(){
        document.getElementById('container-formulario-response').classList.remove('success');
        document.getElementById('body').classList.remove('scroll-block');
    }

    var $ = jQuery  
    var produtos = <?= $produtosJson ?>;
    var quantidadeProdutos = produtos.length;

    function buscarIndexProdutoPelaQuantidade(referencia) {
        produtosLinha = document.getElementsByClassName('linha-produto');
        contadorProdutoLinha = produtosLinha.length
        for(i = 0; i , contadorProdutoLinha; i++){
            if(produtosLinha[i].id == 'linha-produto-'+referencia){
                return i;
            }
        }
    }

  function aumentarDiminuirQuantidadeProdutoA(referencia, tipoOperacao) {
        var inputProdutoSelecionado = $('#quantidade-produto-'+referencia);
    
        var index = buscarIndexProdutoPelaQuantidade(referencia)
        if(tipoOperacao === '-') {
            if(produtos[index].quantidade !== 0) {
                produtos[index].quantidade--
            }
            if(produtos[index].quantidade === 0) {
                cancelarProduto(referencia)
                return
            }
        }
        if(tipoOperacao === '+') {
            produtos[index].quantidade++
        }

        inputProdutoSelecionado.val(produtos[index].quantidade)
        atualizarCarrinho(produtos, '<?= admin_url('admin-ajax.php') ?>')

    }

    function cancelarProduto(referencia) {
        var index = buscarIndexProdutoPelaQuantidade(referencia)
        produtos.splice(index, 1)
        $('#linha-produto-'+referencia).remove();
        atualizarCarrinho(produtos, '<?= admin_url('admin-ajax.php') ?>')
    }

    window.onload = function () {atualizarCarrinho(produtos, '<?= admin_url('admin-ajax.php') ?>');}

</script>
<?php get_footer();?>
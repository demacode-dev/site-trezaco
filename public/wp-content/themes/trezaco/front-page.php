<?php get_header();
    wp_enqueue_style('css_front-page', get_stylesheet_directory_uri().'/src/css/front-page.min.css', array(), null, false);
?>
    <div class="container-banner-carrossel-pai">
        <div class="container-banner-carrossel">
            <?php
                $carrossels = get_field('bloco_carrossel')['carrossel'];
                $contadorCarrosel = count($carrossels);
                for($i = 0; $i < $contadorCarrosel; $i++){
                    $carrossel = $carrossels[$i];
            ?>
                <div class="container-banner">
                    <div class="container-background">
                        <img src="<?=$carrossel['imagem_background']['url']?>">
                        <div class="container-gradient"></div>
                    </div>  
                    <div class="container-padrao">
                        <div class="container-conteudo">
                            <h1><?= $carrossel['titulo']?></h1>
                            <p> <?= $carrossel['texto']?></p>
                            <?php if( $carrossel['botao'] ){ ?>
                                <div class="container-botao">
                                    <a href="<?= $carrossel['link_botao']?>"><?= $carrossel['titulo_botao']?></a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="container-arrows">
            <div class="container-padrao">
                <div class="prev-arrow">
                    <img src="<?=get_stylesheet_directory_uri()?>/img/prev-arrow.png">
                </div>
                <div class="next-arrow">
                    <img src="<?=get_stylesheet_directory_uri()?>/img/next-arrow.png">
                </div>
            </div>
        </div>
    </div>
    <div class="container-valores">
        <div class="container-padrao">
            <?php 
                if(!empty(get_field('bloco_valores')['valores'])){
                    $valores = get_field('bloco_valores')['valores'];
                    $contadorValores = count($valores);
                    for($j = 0; $j < $contadorValores; $j++){
                        $valor = $valores[$j];
            ?>  
                <div class="container-valor anime anime-fade" style="transition-delay: .<?=$j?>s">
                    <div class="container-imagem">
                        <img src="<?=$valor['imagem']['url']?>">
                    </div>
                    <div class="container-conteudo">
                        <h1><?=$valor['titulo']?></h1>
                        <p><?=$valor['texto']?>
                            <?php 
                                if(!empty($valor['imagem_adicional']['url'])){
                            ?>
                                <img src="<?=$valor['imagem_adicional']['url']?>">
                            <?php } ?>
                        </p>
                    </div>
                </div>
            <?php } } ?>
        </div>
    </div>
    <div class="container-produtos-pai">
        <div class="container-padrao anime anime-top">
            <h1><?=get_field('bloco_produtos')['titulo']?></h1>
            <div class="container-categorias-pai">
                <div class="container-categorias">
                    <?php 
                        $argsProdutos = array(
                            'post_type'  => 'produtos',
                            'taxonomy' => 'categoria_produto',
                            'parent' => '0',
                            'hide_empty' => false,
                            'orderby'	=> 'meta_value',
                            'order' => "ASC",
                            'meta_key' => 'ordem_categoria'
                        );  
                        $produtosCategorias = get_terms($argsProdutos);

                        $contadorCategorias = count($produtosCategorias);
                        for($i = 0; $i < $contadorCategorias; $i++){
                            $categoriaPai = $produtosCategorias[$i];
                            if($categoriaPai->slug == 'conexoes' || $categoriaPai->slug == 'valvulas'){
                            }else{
                    ?>
                        <P class="texto-controller texto-controller-<?= $i ?> <?= $i == 0 ? 'ativo' : ''?>" onclick="produtosController('<?= $i ?>')"><?=$categoriaPai->name?></P>
                    <?php } }?>
                </div>
            </div>
            <div class="container-categorias-filhas-pai">
                <?php
                    for($i = 0; $i < $contadorCategorias; $i++){
                        $categoriaPai = $produtosCategorias[$i];
                ?>
                    <div class="container-categorias-filhas-slide categorias-filhas-<?= $i ?> <?=$i == 0 ? 'ativo' : ''?>">
                        <div class="container-categorias-filhas">
                            <?php
                                $argsCategoriasFilhas = array(
                                    'post_type'  => 'produtos',
                                    'parent' => $categoriaPai->term_id,
                                    'post_status' => 'publish',
                                    'orderby'	=> 'name',
                                    'taxonomy' => 'categoria_produto',
                                    'hide_empty' => false
                                );
                                $categoriasFilhas = get_terms($argsCategoriasFilhas);
                                $contadorCategoriasFilhas = count($categoriasFilhas);
                                for($j = 0; $j < $contadorCategoriasFilhas; $j++){
                                    $categoriaFilha = $categoriasFilhas[$j];
                            ?>
                                <div class="container-categoria-filha-pai">
                                    <a href="/produtos/categoria/<?=$categoriaPai->slug?>/<?=$categoriaFilha->slug?>">
                                        <div class="container-categoria-filha">
                                            <img src="<?=!empty(get_field('imagem_em_destaque', $categoriaFilha)['url']) ? get_field('imagem_em_destaque', $categoriaFilha)['url'] : get_stylesheet_directory_uri().'/img/iron-bar.svg'?>">
                                            <h1><?=$categoriaFilha->name?></h1>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="container-fazer-orcamento">
        <div class="container-background">
            <div class="gradient"></div>
            <img src="<?=get_field('bloco_fazer_orcamento')['imagem_background']['url']?>">
        </div>
        <div class="container-padrao">
            <div class="container-conteudo-pai">
                <div class="container-selos">
                    <?php 
                        if(!empty(get_field('bloco_fazer_orcamento')['selos'])){
                            $selos = get_field('bloco_fazer_orcamento')['selos'];
                            $contadorSelos = count($selos);
                            for($j = 0; $j < $contadorSelos; $j++){
                                $selo = $selos[$j];
                    ?>
                        <div class="container-imagem anime anime-left" style="transition-delay: .<?=$j?>s">
                           <img src="<?=$selo['imagem']['url']?>">
                        </div>
                    <?php } } ?>
                </div>
                <h1 class="anime anime-left"><?=get_field('bloco_fazer_orcamento')['titulo']?></h1>
                <p class="anime anime-left"><?=get_field('bloco_fazer_orcamento')['texto']?></p>
                <div class="container-link anime anime-left">
                    <a href="/produtos">Fazer orçamento</a>
                </div>
            </div>
        </div>
    </div>
    <?php 
        if(!get_field('bloco_segmentos')['segmentos_checkbox'] == true){
    ?>
        <div class="container-segmentos">
            <div class="container-padrao anime anime-top">
                <h1  class="titulo"><?=get_field('bloco_segmentos')['titulo']?></h1>
                <div class="container-carrossel-pai">
                    <div class="container-carrossel">
                        <div class="prev-arrow-segmentos">
                            <img src="<?=get_stylesheet_directory_uri()?>/img/orange-prev-arrow.svg">
                        </div>
                        <div class="container-carrossel-nav">
                            
                            <?php 
                                $segmentosArgs = array(
                                    'post_type' => 'segmentos',
                                    'orderby' => 'name',
                                    'post_status' => 'publish',
                                    'order' => 'ASC',
                                    'hide_empty' => false,
                                    'posts_per_page' => -1
                                );
                                $segmentos = get_posts($segmentosArgs);
                                $contadorSegmentos = count($segmentos);
                                for($i = 0; $i < $contadorSegmentos; $i++){
                                    $segmento = $segmentos[$i];
                            ?>
                                <div class="container-segmento-nav-pai">
                                    <div class="container-segmento-nav" style="background-image: url('<?= get_field('imagem_destaque', $segmento->ID)['url']?>')"> </div>
                                    <h1><?=get_field('titulo', $segmento->ID)?></h1>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="next-arrow-segmentos">
                            <img src="<?=get_stylesheet_directory_uri()?>/img/orange-next-arrow.svg">
                        </div>
                        <div class="container-carrossel-content">
                            <?php
                                for($i = 0; $i < $contadorSegmentos; $i++){
                                    $segmento = $segmentos[$i];
                            ?>
                                <div class="container-segmento-content">
                                    <p><?= get_field('texto', $segmento->ID)?></p>
                                    <div class="container-tags">
                                        <?php 
                                            if(!empty(get_the_tags($segmento->ID))){
                                                $tags = get_the_tags($segmento->ID);
                                                $contadorTags = count($tags);
                                                for($j = 0; $j < $contadorTags; $j++){
                                                    $tag = $tags[$j];
                                        ?>
                                            <div class="container-tag">
                                                <p><?= $tag->name ?></p>
                                            </div>
                                        <?php } } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="container-blog-pai" style="background-image: url('<?=get_field('bloco_blog')['imagem_background']['url']?>')">
        <div class="container-padrao  anime anime-top">
            <h1><?=get_field('bloco_blog')['titulo']?></h1>
            <div class="container-blog">
                <?php 
                    $postsBlog = array(
                        'post_type' => 'blog',
                        'orderby' => 'name',
                        'post_status' => 'publish',
                        'order' => 'ASC',
                        'hide_empty' => false,
                        'posts_per_page' => 3,
                        'numberposts' => 3
                    );
                    $blog = get_posts($postsBlog);
                    $contadorBlog = count($blog);
                    for($i = 0; $i < $contadorBlog; $i++){
                        $postBlog = $blog[$i];
                ?>
                    <div class="container-post anime anime-top">
                        <div class="container-imagem">
                            <img src="<?=get_field('imagem_destaque', $postBlog->ID)['url']?>">
                            <div class="container-tag">
                                <p></p>
                            </div>
                        </div>
                        <h1>
                            <?php 
                                if(strlen(get_field('titulo', $postBlog->ID)) > 40){
                                    echo substr(get_field('titulo', $postBlog->ID),0, 40)."..."; 
                                }else{
                                    echo get_field('titulo', $postBlog->ID); 	
                                }
							?>
                        </h1>
                        <p>
                            <?php 
                                if(strlen(get_field('texto', $postBlog->ID)) > 120){
                                    echo substr(get_field('texto', $postBlog->ID),0, 120)."..."; 
                                }else{
                                    echo get_field('texto', $postBlog->ID); 	
                                }
							?>
                        </p>
                        <div class="container-ler-mais">
                            <a href="/blog/<?= $postBlog->post_name?>">Ler postagem</a>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <a id="ver-mais" href="/blog">Ver mais postagens</a>
        </div>
    </div>
    <script>
        jQuery(document).ready(function($){ 
            $('.container-banner-carrossel').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: true,
                dots: true,
                infinite: false,
                prevArrow: $('.prev-arrow'),
                nextArrow: $('.next-arrow'),
                customPaging: function(slider, i) { 
                    return '<button class="dot"></button>';
                }
            });
            $('.container-categorias-filhas').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: true,
                infinite: true,
                responsive: [
                    {
                    breakpoint: 950,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                    },
                    {
                    breakpoint: 650,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                    },
                    {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                ]
            });
            $('.container-carrossel-content').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.container-carrossel-nav'
            });
            $('.container-carrossel-nav').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.container-carrossel-content',
                centerMode: true,
                focusOnSelect: true,
                dots: false,
                arrows: true,
                prevArrow: $('.prev-arrow-segmentos'),
                nextArrow: $('.next-arrow-segmentos'),
                responsive: [
                    {
                    breakpoint: 550,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                ]
            });
        });
    </script>
<?php get_footer(); ?>
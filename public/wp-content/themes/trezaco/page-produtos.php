<?php
    wp_enqueue_style('css_produtos', get_stylesheet_directory_uri().'/src/css/produtos.min.css', array(), null, false);
    get_header();
    
    $categoriaPaiQuery = get_query_var('categoriaprodutos', '');
    $categoriaFilhaQuery = get_query_var('subcategoriaprodutos', '');
    $index_paginacao = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
?>
<div class="container-breadcrumb">
    <div class="container-padrao">
        <img class="home" src="<?php echo get_stylesheet_directory_uri()?>/img/home-solid.svg">
        <img class="arrow" src="<?php echo get_stylesheet_directory_uri()?>/img/orange-next-arrow.svg">
        <a href="/produtos">Produtos</a>
    </div>
</div>
<div class="container-download">
    <div class="container-padrao">
        <a href="<?= get_field('pfd_medidas')['url']?>" download="MedidasTrezaço"><?= get_field('texto_botao_download')?></a>
    </div>
</div>
<div class="container-produtos-categorias-pai">
    <div class="container-padrao">
        <div class="container-categorias-pai">
            <div class="container-categorias">
                <?php 
                    $argsProdutos = array(
                        'post_type'  => 'produtos',
                        'taxonomy' => 'categoria_produto',
                        'parent' => '0',
                        'hide_empty' => false,
                        'orderby'	=> 'meta_value',
                        'order' => "ASC",
                        'meta_key' => 'ordem_categoria'
                    );  
                    $produtosCategorias = get_terms($argsProdutos);

                    $contadorCategorias = count($produtosCategorias);
                    for($i = 0; $i < $contadorCategorias; $i++){
                        $categoriaPai = $produtosCategorias[$i];
                        if($categoriaPai->slug == 'conexoes' || $categoriaPai->slug == 'valvulas'){
                        }else{
                ?>
                    <a href="/produtos/categoria/<?=$categoriaPai->slug?>" class="texto-controller texto-controller-<?= $i ?> <?= ($categoriaPaiQuery == $categoriaPai->slug || ($categoriaPaiQuery == '' && $i == 0)) ? 'ativo' : ''?>"><?=$categoriaPai->name?></a>
                <?php } }?>
            </div>
        </div>
        <div class="container-categorias-filhas-pai">
            <div class="container-categorias-filhas-slide categorias-filhas-<?= $i ?> <?=($categoriaPaiQuery == $categoriaPai->slug || ($categoriaPaiQuery == '' && $i == 0)) ? 'ativo' : ''?>">
                <div class="container-categorias-filhas">
                    <?php
                        if($categoriaPaiTerm = get_term_by('slug', $categoriaPaiQuery, 'categoria_produto')){
                            $categoriaPaiTerm = get_term_by('slug', $categoriaPaiQuery, 'categoria_produto');
                        }else{
                            $categoriaPaiTerm = $produtosCategorias[0];
                        }
                        

                        $argsCategoriasFilhas = array( 
                            'post_type'  => 'produtos',
                            'parent' => $categoriaPaiTerm->term_id,
                            'post_status' => 'publish',
                            'orderby'	=> 'name',
                            'taxonomy' => 'categoria_produto',
                            'hide_empty' => false
                        );
                        $categoriasFilhas = get_terms($argsCategoriasFilhas);
                        $contadorCategoriasFilhas = count($categoriasFilhas);
                        for($j = 0; $j < $contadorCategoriasFilhas; $j++){
                            $categoriaFilha = $categoriasFilhas[$j];
                    ?>
                        <div class="container-categoria-filha-pai">
                            <a href="/produtos/categoria/<?=$categoriaPaiTerm->slug?>/<?=$categoriaFilha->slug?>">
                                <div class="container-categoria-filha <?=$categoriaFilhaQuery == $categoriaFilha->slug ? 'ativo' : ''?>">
                                    <img src="<?= !empty(get_field('imagem_em_destaque', $categoriaFilha)['url']) ? get_field('imagem_em_destaque', $categoriaFilha)['url'] : get_stylesheet_directory_uri().'/img/iron-bar.svg' ?>">
                                    <h1><?=$categoriaFilha->name?></h1>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-produtos-pai">
    <div class="container-padrao">
        <?php 

            $taxQuery = array();
            $taxArg = 'tubos';
            $postPorPagina = 6;
             
            if(!empty($categoriaPaiQuery)){
                $taxArg = $categoriaPaiQuery;
            }
            if(!empty($categoriaFilhaQuery)){
                $taxArg = $categoriaFilhaQuery;
            }

            $taxQuery =  array(
                array(
                    'taxonomy' => 'categoria_produto',
                    'field' => 'slug',
                    'terms' => $taxArg
                )
            );

            $argsProdutos = array(
                'post_type'  => 'produtos',
                'orderby' => 'date',
                'order' => 'DESC',
                'parent' => '0',
                'hide_empty' => false,
                'posts_per_page' => $postPorPagina,
                'tax_query' => $taxQuery,
                'nopaging' => false,
                'paged' => $index_paginacao
            );  
            $produtos = get_posts($argsProdutos);
            if(!empty($produtos)){
                $contadorProdutos = count($produtos);
                for($j = 0; $j < $contadorProdutos; $j++){
                    $produto = $produtos[$j];
                    $codigoReferencia = get_field('codigo_de_referencia', $produto);
                    $imagemUrl = get_field('imagem_do_produto', $produto)['url'];
                    $tituloProduto = get_field('titulo_do_produto', $produto);

                    $tipoMedidaPost = get_field('tipo_de_medida', $produto);
                    $tipoMedidaCategoria = get_field('tipo_do_produto', $tipoMedidaPost);
                    $tipoMedidaSubcategoria = get_field('tipo_de_' . $tipoMedidaCategoria, $tipoMedidaPost);

                    $medidasArray = get_field('medidas_' . $tipoMedidaCategoria . '_' . $tipoMedidaSubcategoria, $tipoMedidaPost);

                    if(isset($medidasArray['medida_especial'])){
                        $medidas = $medidasArray['medida_especial'];
                    }else{
                        $medidas = $medidasArray['medida_x_medida'];
                    }
                    
                    $arrayTituloMedidas = "";
                    $tituloMedida1 = $medidasArray['nome_medida_1'];
                    $tituloMedida2 = "";

                    $arrayTituloMedidas .= $tituloMedida1;
                    if(!empty($medidasArray['nome_medida_2'])){
                        $tituloMedida2 = $medidasArray['nome_medida_2'];
                        $arrayTituloMedidas .=  '-'.$tituloMedida2;
                    }
                    $tituloMedida3 = "";
                    if(!empty($medidasArray['nome_medida_3'])){
                        $tituloMedida3 = $medidasArray['nome_medida_3'];
                        $arrayTituloMedidas .=  '-'.$tituloMedida3;
                    }
                    $tituloMedida4 = "";
                    if(!empty($medidasArray['nome_medida_4'])){
                        $tituloMedida4 = $medidasArray['nome_medida_4'];
                        $arrayTituloMedidas .=  '-'.$tituloMedida4;
                    }
                    $tituloMedida5 = "";
                    if(!empty($medidasArray['nome_medida_5'])){
                        $tituloMedida5 = $medidasArray['nome_medida_5'];
                        $arrayTituloMedidas .=  '-'.$tituloMedida5;
                    }

        ?>
        <div class="container-produto-pai">
            <span>Ref. <?=$codigoReferencia?></span>
            <h1><?=$produto->post_title?></h1>
            <p><?=get_field('descricao', $produto)?></p>
            <div class="container-produto">
                <div class="container-imagem">
                    <img src="<?=$imagemUrl?>">
                </div>
                <div class="container-info">
                    <div div class="container-medidas-pai">
                        <?php
                            if($medidas == 'especial'){
                        ?>
                            <div class="container-especial-pai-titulo">
                                <p><?=$tituloMedida1?></p>
                                <div class="container-especial-pai">
                                    <p hidden id="texto-selected-<?=$produto->ID?>"></p>
                                    <input oninput="medidasEspecialController(this.value, '<?=$produto->ID?>')" placeholder="Digite o valor da medida">
                                </div>
                            </div>
                            <div class="container-especial-filho-titulo">
                                <p><?=$tituloMedida2?></p>
                                <div class="container-especial-filho">
                                    <p hidden class="texto-selected-filha-<?=$produto->ID?> ativo" id="texto-selected-filha-<?=$produto->ID?>"></p>
                                    <input oninput="medidasFilhaEspecialController(this.value, '<?=$produto->ID?>')" placeholder="Digite o valor da medida">
                                </div>
                            </div>
                        <?php
                            }else{
                        ?>
                            <div class="container-seletor-pai-titulo">
                                <p><?=$tituloMedida1?></p>
                                <div class="container-seletor-pai">
                                    <div class="container-seletor">
                                        <div class="container-selected" onclick="abrirDropDown('<?=$produto->ID?>')">
                                            <p id="texto-selected-<?=$produto->ID?>"><?= $medidas[0]['medida_1']?></p>
                                            <div class="container-seta">
                                                <img id="seta-<?=$produto->ID?>" src="<?php echo get_template_directory_uri()?>/img/orange-down-arrow.svg">
                                            </div>
                                        </div>
                                        <div class="container-options" id="container-options-<?=$produto->ID?>">
                                            <?php
                                                $contadorMedidas = count($medidas);
                                                for($k = 0; $k < $contadorMedidas; $k++){
                                                    $medidaPai = $medidas[$k];
                                                    $medidaValor = $medidaPai['medida_1'];
                                                    $medidaValor =  str_replace('"', '&quot', $medidaValor);
                                            ?>
                                                <div class="option" onclick="medidasController('<?= $medidaValor?>', '<?=$k?>', '<?=$produto->ID?>')"><?= $medidaPai['medida_1']?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                                for($k = 0; $k < $contadorMedidas; $k++){
                                    $medida = $medidas[$k];
                            ?>
                                <div class="container-medidas container-medidas-<?=$produto->ID?> container-medidas-<?=$produto->ID?>-<?=$k?> <?=$k == 0 ? 'ativo' : ''?>">
                                    <?php
                                        if(isset($medida['medidas_2']) && !empty($medida['medidas_2'])){
                                    ?>
                                        <div class="container-seletor-filho-pai">
                                            <p><?=$tituloMedida2?></p>
                                            <div class="container-seletor-filho">
                                                <div class="container-seletor">
                                                    <div class="container-selected" onclick="abrirDropDownFilha('<?=$produto->ID?>', 'medida-2', '<?=$k?>')">
                                                        <p class="texto-selected-filha-<?=$produto->ID?> <?=$k == 0 ? 'ativo' : ''?> texto-selected-filha-<?=$produto->ID?>-<?=$k?> " id="texto-selected-filha-medida-2-<?=$produto->ID?>-<?=$k?>"><?= $medida['medidas_2'][0]['medida_2']?></p>
                                                        <div class="container-seta">
                                                            <img id="seta-<?=$produto->ID?>-medida-2-<?=$k?>" src="<?php echo get_template_directory_uri()?>/img/orange-down-arrow.svg">
                                                        </div>
                                                    </div>
                                                    <div class="container-options" id="container-options-<?=$produto->ID?>-medida-2-<?=$k?>">
                                                        <?php
                                                            $medidasFilha2 = $medida['medidas_2'];
                                                            $contadorMedidaFilha2 = count($medidasFilha2);
                                                            for($i = 0; $i < $contadorMedidaFilha2; $i++){
                                                                $medidaFilha2 = $medidasFilha2[$i];
                                                                $medidaFilha2Valor = $medidaFilha2['medida_2'];
                                                                $medidaFilha2Valor =  str_replace('"', '&quot', $medidaFilha2Valor);
                                                        ?>
                                                            <div class="option-pai" onclick="medidaValorController('<?= $medidaFilha2Valor?>','medida-2', '<?=$produto->ID?>', '<?= $k?>')">
                                                                <div class="option"><?= $medidaFilha2['medida_2']?></div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                        if(isset($medida['medidas_3']) && !empty($medida['medidas_3'])){
                                    ?>
                                        <div class="container-seletor-filho-pai">
                                            <p><?=$tituloMedida3?></p>
                                            <div class="container-seletor-filho">
                                                <div class="container-seletor">
                                                    <div class="container-selected" onclick="abrirDropDownFilha('<?=$produto->ID?>', 'medida-3', '<?=$k?>')">
                                                        <p class="texto-selected-filha-<?=$produto->ID?> <?=$k == 0 ? 'ativo' : ''?> texto-selected-filha-<?=$produto->ID?>-<?=$k?> " id="texto-selected-filha-medida-3-<?=$produto->ID?>-<?=$k?>"><?= $medida['medidas_3'][0]['medida_3']?></p>
                                                        <div class="container-seta">
                                                            <img id="seta-<?=$produto->ID?>-medida-3-<?=$k?>" src="<?php echo get_template_directory_uri()?>/img/orange-down-arrow.svg">
                                                        </div>
                                                    </div>
                                                    <div class="container-options" id="container-options-<?=$produto->ID?>-medida-3-<?=$k?>">
                                                        <?php
                                                            $medidasFilha3 = $medida['medidas_3'];
                                                            $contadorMedidaFilha3 = count($medidasFilha3);
                                                            for($i = 0; $i < $contadorMedidaFilha3; $i++){
                                                                $medidaFilha3 = $medidasFilha3[$i];
                                                                $medidaFilha3Valor = $medidaFilha3['medida_3'];
                                                                $medidaFilha3Valor =  str_replace('"', '&quot', $medidaFilha3Valor);
                                                        ?>
                                                            <div class="option-pai" onclick="medidaValorController('<?= $medidaFilha3Valor?>','medida-3', '<?=$produto->ID?>', '<?= $k?>')">
                                                                <div class="option"><?= $medidaFilha3['medida_3']?></div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                        if(isset($medida['medidas_4']) && !empty($medida['medidas_4'])){
                                    ?>
                                        <div class="container-seletor-filho-pai">
                                            <p><?=$tituloMedida4?></p>
                                            <div class="container-seletor-filho">
                                                <div class="container-seletor">
                                                    <div class="container-selected" onclick="abrirDropDownFilha('<?=$produto->ID?>', 'medida-4', '<?=$k?>')">
                                                        <p class="texto-selected-filha-<?=$produto->ID?> <?=$k == 0 ? 'ativo' : ''?> texto-selected-filha-<?=$produto->ID?>-<?=$k?> " id="texto-selected-filha-medida-4-<?=$produto->ID?>-<?=$k?>"><?= $medida['medidas_4'][0]['medida_4']?></p>
                                                        <div class="container-seta">
                                                            <img id="seta-<?=$produto->ID?>-medida-4-<?=$k?>" src="<?php echo get_template_directory_uri()?>/img/orange-down-arrow.svg">
                                                        </div>
                                                    </div>
                                                    <div class="container-options" id="container-options-<?=$produto->ID?>-medida-4-<?=$k?>">
                                                        <?php
                                                            $medidasFilha4 = $medida['medidas_4'];
                                                            $contadorMedidaFilha4 = count($medidasFilha4);
                                                            for($i = 0; $i < $contadorMedidaFilha4; $i++){
                                                                $medidaFilha4 = $medidasFilha4[$i];
                                                                $medidaFilha4Valor = $medidaFilha4['medida_4'];
                                                                $medidaFilha4Valor =  str_replace('"', '&quot', $medidaFilha4Valor);
                                                        ?>
                                                            <div class="option-pai" onclick="medidaValorController('<?= $medidaFilha4Valor?>','medida-4', '<?=$produto->ID?>', '<?= $k?>')">
                                                                <div class="option"><?= $medidaFilha4['medida_4']?></div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="container-quantidade-pai">
                        <p>Quantidade</p>
                        <div class="quantidade-produto">
                            <button onclick="aumentarDiminuirQuantidadeProduto('-', <?=$j?>)">
                                -
                            </button>
                            <input type="number" class="quantidade-produto-input" id="quantidade-<?=$j?>" readonly="readonly" value="1">
                            <button onclick="aumentarDiminuirQuantidadeProduto('+', <?=$j?>)">
                                +
                            </button>
                        </div>
                    </div>
                    <div class="container-adicionar">
                        <a onclick="adicionarAoOrcamento('<?= $codigoReferencia?>', '<?= $tituloProduto ?>', '<?= $imagemUrl?>', '<?= $arrayTituloMedidas?>', '<?=$produto->ID?>', document.getElementById('quantidade-'+<?=$j?>).value, '<?= admin_url('admin-ajax.php') ?>')">Adicionar ao orçamento</a>
                    </div>
                    <div class="container-cartao">
                        <img src="<?=get_stylesheet_directory_uri()?>/img/pay-card.svg">
                        <p>Aceitamos cartões de crédito, débito, cartão BNDES e boleto.</p>
                    </div>
                </div>
            </div>
        </div>
       
        <?php } }?>
    </div>
</div>
<?php 
    $argsProdutosCountTotal = array(
        'post_type'  => 'produtos',
        'orderby' => 'date',
        'order' => 'DESC',
        'parent' => '0',
        'hide_empty' => false,
        'posts_per_page' => -1 ,
        'tax_query' => $taxQuery
    );  
    $produtosCountTotal = get_posts($argsProdutosCountTotal);
    $contadorProdutosTotal = count($produtosCountTotal);
    if($contadorProdutosTotal > $postPorPagina){ ?>
        <div class="container-paginacao">
            <div class="paginacao">
                <?php 
                    echo paginate_links( 
                        array(
                            'base' => preg_replace('/\?.*/', '/', get_pagenum_link()) . '%_%',
                            'format' => '?pagina=%#%',
                            'current' => $index_paginacao,
                            'total' => ceil($contadorProdutosTotal / $postPorPagina),
                            'prev_next' => true,
                            'mid_size' => 1,
                            'type' => 'plain',
                            'next_text' => '>',
                            'prev_text' => '<',
                        ) 
                    );
                ?>
            </div>
        </div>


    <?php } ?>
<div class="container-produtos-info">
    <div class="container-padrao">
        <h1><?=get_field('titulo', $categoriaPaiTerm)?></h1>
        <div class="container-conteudo">
            <p><?=$categoriaPaiTerm->description?></p>
        </div>
    </div>
</div>
<div class="container-valores">
    <div class="container-padrao">
        <?php 
            if(!empty(get_field('bloco_valores')['valores'])){
                $valores = get_field('bloco_valores')['valores'];
                $contadorValores = count($valores);
                for($j = 0; $j < $contadorValores; $j++){
                    $valor = $valores[$j];
        ?>  
            <div class="container-valor">
                <div class="container-imagem">
                    <img src="<?=$valor['imagem']['url']?>">
                </div>
                <div class="container-conteudo">
                    <h1><?=$valor['titulo']?></h1>
                    <p><?=$valor['texto']?>
                        <?php 
                            if(!empty($valor['imagem_adicional']['url'])){
                        ?>
                            <img src="<?=$valor['imagem_adicional']['url']?>">
                        <?php } ?>
                    </p>
                </div>
            </div>
        <?php } } ?>
    </div>
</div>
<div class="container-response-pai" id="container-response-pai">
    <div class="container-padrao">
        <div class="container-conteudo">
            <div class="container-response">
                <img src="<?=get_stylesheet_directory_uri()?>/img/success-white.svg">
                <h1>Adicionado com sucesso à  lista de orçamentos </h1>
            </div>
            <div class="container-botoes">
                <p onclick="modalSuccessFechar()">Continuar orçando</p>
                <a href="/orcamento">Ir até orçamentos</a>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function($){ 
        $('.container-categorias-filhas').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            infinite: true,
            responsive: [
                {
                breakpoint: 950,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
                },
                {
                breakpoint: 650,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
                },
                {
                breakpoint: 450,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }
            ]
        });
    });
</script>
<?php get_footer();?>
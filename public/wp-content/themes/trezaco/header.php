<?php  do_action('before_wphead_after_init');?>
<!DOCTYPE html>
<html <?php language_attributes();?> class="no-js">
<head>

	<?php
		HC::init();
	?>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0" />
	<title><?php echo wp_title();?></title>
    <!-- FONT AWESOME - ICONES PERSONALIZADOS -->
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- FONTE DO SITE -->
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/header.min.css">
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/footer.min.css">
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/carrinho.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
	<link href="https://fonts.googleapis.com/css2?family=Libre+Franklin:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<?php wp_head();?>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120640110-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-120640110-1');
	</script>

</head>

<body id="body" class="scroll-body scroll-block">
<div class="container-loading" id="container-loading">
	<div class="conteudo loading">
		<img id="logo" src="<?=get_field('logo_empresa', 'header')['url']?>">
		<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" style="height: 50px; width: 50px; background: rgb(255, 255, 255, 0);" width="128px" height="128px">
			<g class="ldl-scale" style="transform-origin: 50% 50%; transform: rotate(0deg) scale(0.8, 0.8);">
				<g class="ldl-ani">
					<g class="ldl-layer">
						<g class="ldl-ani">
							<g>
								<g class="ldl-layer">
									<g class="ldl-ani" style="transform-origin: 50px 50px; transform: rotate(0deg); animation: 2.5s cubic-bezier(0.4, 0.46, 0.28, 0.97) -0.740741s infinite normal forwards running teste;">
										<path fill="#666" d="M89.764 60.828l-3.99 9.633a3.09 3.09 0 0 1-4.038 1.673l-3.483-1.443c-1.211-.501-2.617-.194-3.476.795a32.598 32.598 0 0 1-3.286 3.279c-.994.86-1.305 2.268-.803 3.482l1.445 3.489a3.09 3.09 0 0 1-1.673 4.038l-9.633 3.99a3.09 3.09 0 0 1-4.038-1.673l-1.445-3.489c-.503-1.214-1.719-1.99-3.03-1.895a32.54 32.54 0 0 1-4.642.005c-1.307-.092-2.519.685-3.02 1.896l-1.443 3.483a3.09 3.09 0 0 1-4.038 1.673l-9.633-3.99a3.09 3.09 0 0 1-1.673-4.038l1.443-3.483c.501-1.211.194-2.617-.795-3.476a32.598 32.598 0 0 1-3.279-3.286c-.86-.994-2.268-1.305-3.482-.803l-3.489 1.445a3.09 3.09 0 0 1-4.038-1.673l-3.99-9.633a3.09 3.09 0 0 1 1.673-4.038l3.489-1.445c1.214-.503 1.99-1.719 1.895-3.03a32.54 32.54 0 0 1-.005-4.642c.092-1.307-.685-2.519-1.896-3.02l-3.483-1.443a3.09 3.09 0 0 1-1.673-4.038l3.99-9.633a3.09 3.09 0 0 1 4.038-1.673l3.483 1.443c1.211.501 2.617.194 3.476-.795a32.598 32.598 0 0 1 3.286-3.279c.994-.86 1.305-2.268.803-3.482l-1.445-3.489a3.09 3.09 0 0 1 1.673-4.038l9.633-3.99a3.09 3.09 0 0 1 4.038 1.673l1.445 3.489c.503 1.214 1.719 1.99 3.03 1.895a32.54 32.54 0 0 1 4.642-.005c1.307.092 2.519-.685 3.02-1.896l1.443-3.483a3.09 3.09 0 0 1 4.038-1.673l9.633 3.99a3.09 3.09 0 0 1 1.673 4.038l-1.443 3.483c-.501 1.211-.194 2.617.795 3.476a32.598 32.598 0 0 1 3.279 3.286c.86.994 2.268 1.305 3.482.803l3.489-1.445a3.09 3.09 0 0 1 4.038 1.673l3.99 9.633a3.09 3.09 0 0 1-1.673 4.038l-3.489 1.445c-1.214.503-1.99 1.719-1.895 3.03a32.54 32.54 0 0 1 .005 4.642c-.092 1.307.685 2.519 1.896 3.02l3.483 1.443a3.09 3.09 0 0 1 1.673 4.038z" style="fill: rgb(102, 102, 102);"></path>
									</g>
								</g>
								<g class="ldl-layer">
									<g class="ldl-ani" style="transform-origin: 50px 50px; transform: rotate(0deg); animation: 2.5s cubic-bezier(0.4, 0.46, 0.28, 0.97) -0.740741s infinite normal forwards running teste;">
										<circle fill="#323232" r="24.4" cy="50" cx="50" style="fill: rgb(50, 50, 50);">

										</circle>
									</g>
								</g>
								<g class="ldl-layer">
									<g class="ldl-ani" style="transform-origin: 50px 50px; transform: rotate(0deg); animation: 2.5s cubic-bezier(0.4, 0.46, 0.28, 0.97) -0.740741s infinite normal forwards running teste;">
										<circle r="15.145" cy="50" cx="50" style="fill: rgb(0, 0, 0);">

										</circle>
									</g>
								</g>
							</g>
						</g>
					</g>
				</g>
			</g>
		</svg>
	</div>
</div>
<div class="container-contato-pai">
	<div class="container-padrao">
		<div class="container-contato">
			<p><img src="<?=get_stylesheet_directory_uri()?>/img/email.svg"><?=get_field('email', 'header')?></p>
			<p><img src="<?=get_stylesheet_directory_uri()?>/img/phone.svg"><?=get_field('telefone', 'header')?></p>
			<p><img src="<?=get_stylesheet_directory_uri()?>/img/clock.svg"><?=get_field('horario_de_funcionamento', 'header')?></p>
		</div>
		<div class="container-boleto">
			<a target="_blank" href="<?=get_field('link_topo', 'header')?>"><?=get_field('titulo_link_topo', 'header')?></a>
		</div>
	</div>
</div>
<header class="header" id="home">
	<div class="menu-lateral" id="menu-lateral">
		<div class="after-container">
			<div class="fechar-menu">
				<img onclick="abrirMenu()" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cancel.svg" alt="">
				<a href="/"><img class="logo" src="<?=get_field('logo_empresa', 'header')['url']?>"></a>
				<div class="div-vazia"></div>
			</div>
			<div class="itens-responsivo">
				<ul>
					<li><a href="/">Home</a></li>
					<li><a href="/produtos">Produtos</a></li>
					<li><a href="/sobre-nos">Sobre nós</a></li>
					<li><a href="/normas">Normas</a></li>
					<li><a href="/blog">Blog</a></li>
					<li><a href="/contato">Contato</a></li>
					<li><a id="fazer-orcamento-li" href="/produtos">Fazer orçamento</a></li>
				</ul>
			</div>
			<div class="container-redes-pai">
				<div class="container-redes">
					<?php
						$redes = get_field('redes_sociais', 'header');
						$contadorRedes = count($redes);
						for($i =0; $i < $contadorRedes; $i++){
							$rede = $redes[$i];
					?>
					<a target="_blank" href="<?= $rede['link']?>"><img src="<?= $rede['icone']['url']?>"></a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="container-logo-pai">
		<div class="container-padrao">
			<div class="container-redes">
				<?php
					$redes = get_field('redes_sociais', 'header');
					$contadorRedes = count($redes);
					for($i =0; $i < $contadorRedes; $i++){
						$rede = $redes[$i];
				?>
				<a target="_blank" href="<?= $rede['link']?>"><img src="<?= $rede['icone']['url']?>"></a>
				<?php } ?>
			</div>
			<div class="container-menu-responsivo" id="container-menu-responsivo">
				<img onclick="abrirMenu()" src="<?= get_stylesheet_directory_uri()?>/img/bars-solid.svg">
			</div>
			<a href="/" id="logo-header">
				<div class="container-logo">
					<img src="<?=get_field('logo_empresa', 'header')['url']?>">
				</div>
			</a>
			<div class="container-botoes">
				<div class="container-pesquisa">
					<img onclick="pesquisaController()" src="<?=get_stylesheet_directory_uri()?>/img/lupa.png">
					<input id="input-busca" type="text" placeholder="Pesquisa no blog">
				</div>
				<div class="container-botao">
					<a href="/orcamento">
						<img src="<?=get_stylesheet_directory_uri()?>/img/prancheta.png">
					</a>
					<div class="quantidade-carrinho" id="quantidade-carrinho">
						<?= empty($_SESSION['carrinho']) ? "0" : count($_SESSION['carrinho']) ?>
					</div>
					<div class="carrinho-lista" id="container-carrinho">
						<?php
							if(!empty($_SESSION["carrinho"])){
								$carrinhoProdutosEscolhidos = $_SESSION["carrinho"];
								$carrinhoQuantidadeProdutosEscolhidos = count($carrinhoProdutosEscolhidos);
								$carrinhoProdutosJson = json_encode($carrinhoProdutosEscolhidos);
								$carrinhoQuantidadeProdutosEscolhidosTotal = count($carrinhoProdutosEscolhidos);
							}else{
								$carrinhoProdutosEscolhidos = '';
								$carrinhoQuantidadeProdutosEscolhidos = 0;
								$carrinhoProdutosJson = 'null';
							}
						?>
						<div class="container-conteudo container-carrinho-ajax">
							<?php if($carrinhoQuantidadeProdutosEscolhidos == 0){ ?>
								<p>Seu carrinho está vazio</p>
							<?php 
								}else{ 
							?>
								<span><?=$carrinhoQuantidadeProdutosEscolhidosTotal?> itens na lista de orçamento</span>
								<div class="container-botao-carrinho">
									<a href="/orcamento">Enviar orçamento</a>
								</div>
							<?php
									$carrinhoQuantidadeProdutosEscolhidos = $carrinhoQuantidadeProdutosEscolhidos < 4 ? $carrinhoQuantidadeProdutosEscolhidos : 3;
									for($i = 0; $i < $carrinhoQuantidadeProdutosEscolhidos; $i++){
										$produtoCarrinho = (object) $carrinhoProdutosEscolhidos[$i];
										$nomeCarrinho = $produtoCarrinho->nome;
										$referenciaCarrinho = $produtoCarrinho->referencia;
										$quantidadeCarrinho = $produtoCarrinho->quantidade;
										$medidas = json_decode(json_encode($produtoCarrinho->medidas), true);
							?>
								<div class="container-produto linha-produto-carrinho" id="linha-produto-carrinho-<?=$i?>">
									<div class="container-info-pai">
										<div class="container-titulo">
											<h1><?=$nomeCarrinho?></h1>
											<p>
												<?php
													foreach($medidas as $key=>$value){
														echo "$key de $value<br />";
													}
												?>
											</p>
										</div>
										<div class="container-info">
											<div class="container-quantidade-pai">
												<div class="container-quantidade">
													<p>Qtd:</p>
													<div class="quantidade-produto">
														<button onclick="aumentarDiminuirQuantidadeProdutoCarrinho('<?= $i ?>', '-')">
															-
														</button>
														<input type="number" class="quantidade-produto-input" id="quantidade-produto-carrinho-<?= $i ?>" readonly="readonly" value="<?=$quantidadeCarrinho?>">
														<button onclick="aumentarDiminuirQuantidadeProdutoCarrinho('<?= $i ?>', '+')">
															+
														</button>
													</div>
												</div>
											</div>
											<!-- <div class="container-remove">
												<div class="container-remove-image" onclick="cancelarProdutoCarrinho('<?= $i ?>')">
													<img src="<?=get_stylesheet_directory_uri()?>/img/lixeira.svg">
												</div>
											</div> -->
										</div>
									</div>
								</div>
							<?php } }?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-pages-pai">
		<div class="container-padrao">
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="/produtos">Produtos</a></li>
				<li><a href="/sobre-nos">Sobre nós</a></li>
				<li><a href="/normas">Normas</a></li>
				<li><a href="/blog">Blog</a></li>
				<li><a href="/contato">Contato</a></li>
				<li id="orcamento"><a href="/produtos">Fazer orçamento</a></li>
			</ul>
		</div>
	</div>
</header>
	<script>
		jQuery("#input-busca").on('keyup', function (e) {
			console.log('key up');
			if (e.keyCode == 13) {
				console.log('key up');
					window.location.href = '<?php echo home_url();?>/blog/pesquisa/' + document.getElementById("input-busca").value;
			}
		});

		var produtosCarrinho = <?= $carrinhoProdutosJson ?>;

		// function cancelarProdutoCarrinho(referencia) {
		// 	var index = buscarIndexProdutoPelaQuantidadeCarrinho(referencia);
		// 	produtosCarrinho.splice(index, 1);
		// 	jQuery('#linha-produto-carrinho-'+referencia).remove();
		// 	if (document.location.pathname.indexOf("/orcamento/") == 0) {
		// 		jQuery('#linha-produto-'+referencia).remove();
		// 	}
		// 	atualizarCarrinho(produtosCarrinho, '<?= admin_url('admin-ajax.php') ?>');
		// }

		// function buscarIndexProdutoPelaQuantidadeCarrinho(referencia) {
		// 	produtosLinha = document.getElementsByClassName('linha-produto-carrinho');
		// 	contadorProdutoLinha = produtosLinha.length;
		// 	for(i = 0; i , contadorProdutoLinha; i++){
		// 		if(produtosLinha[i].id == 'linha-produto-carrinho-'+referencia){
		// 			return i;
		// 		}
		// 	}
		// }
	</script>
	<main>

<?php
    wp_enqueue_style('css_contato', get_stylesheet_directory_uri().'/src/css/contato.min.css', array(), null, false);
    get_header();
?>
<div class="container-background-pai">
    <div class="background">
        <img src="<?=get_field('imagem_background')['url']?>">
    </div>
    <div class="container-breadcrumb">
        <div class="container-padrao">
            <img class="home" src="<?php echo get_stylesheet_directory_uri()?>/img/home-solid.svg">
            <img class="arrow" src="<?php echo get_stylesheet_directory_uri()?>/img/orange-next-arrow.svg">
            <a href="/contato">Contato</a>
        </div>
    </div>
    <div class="container-bloco-contato">
        <div class="container-padrao">
            <h1 class="anime anime-fade"><?=get_field('bloco_contato')['titulo']?></h1>
            <div class="container-contato-sac">
                <div class="container-contato-rapido anime anime-left">
                    <div class="container-imagem">
                        <img src="<?=get_field('bloco_contato')['contato_rapido']['icone']['url']?>">
                    </div>
                    <div class="container-conteudo">
                        <h1><?=get_field('bloco_contato')['contato_rapido']['titulo']?></h1>
                        <p><?=get_field('bloco_contato')['contato_rapido']['texto'] ?></p>
                        <div class="link">
                            <a target="_blank" rel="noopener noreferrer" href="https://api.whatsapp.com/send?phone=<?= get_field('bloco_contato')['contato_rapido']['numero_whatsapp']?>&text=Sua%20mensagem%20aqui">Mandar mensagem</a>
                        </div>
                    </div>
                </div>
                <div class="container-sac anime anime-left" style="transition-delay: .1s;">
                    <div class="container-imagem">
                        <img src="<?=get_field('bloco_contato')['sac']['icone']['url']?>">
                    </div>
                    <div class="container-conteudo">
                        <h1><?=get_field('bloco_contato')['sac']['titulo']?></h1>
                        <?php
                            $telefones = get_field('bloco_contato')['sac']['telefones'];
                            $contadorTelefones = count($telefones);
                            for($i = 0; $i < $contadorTelefones; $i++){
                                $telefone = $telefones[$i];
                        ?>
                            <p><img src="<?= get_stylesheet_directory_uri()?>/img/phone.svg"><?=$telefone['telefone']?></p>
                        <?php } ?>
                        <p><?=get_field('bloco_contato')['sac']['email']?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-faq-pai">
        <div class="container-padrao anime anime-top">
            <h1><?=get_field('bloco_links_uteis')['titulo']?></h1>
            <div class="container-faq">
                <div class="container-faq-titulos">
                    <?php 
                        $argsFaq = array(
                            'post_type' => 'faq',
                            'orderby' => 'name',
                            'post_status' => 'publish',
                            'order' => 'ASC',
                            'hide_empty' => false,
                        );  
                        $faqs = get_posts($argsFaq);
                        $contadorFaqs = count($faqs);
                        for($i = 0; $i < $contadorFaqs; $i++){
                            $faq = $faqs[$i];
                    ?>
                        <P class="faq-controller faq-controller-<?= $i ?> <?= $i == 0 ? 'ativo' : ''?>" onclick="faqController('<?= $i ?>')"><?=get_field('titulo', $faq->ID)?></P>
                    <?php } ?>
                </div>
                <div class="container-faq-conteudo-pai">
                    <?php 
                        for($i = 0; $i < $contadorFaqs; $i++){
                            $faq = $faqs[$i];
                    ?>
                        <div class="container-faq-conteudo faq-conteudo-<?= $i ?> <?= $i == 0 ? 'ativo' : ''?>">
                            <?= get_field('texto', $faq->ID)?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-formulario-pai">
        <div class="container-padrao anime anime-top">
            <h1><?=get_field('bloco_formulario')['titulo']?></h1>
            <div class="container-formulario anime anime-top">
                <h2>Preencha com seus dados de contato</h2>
                <form data-hc-form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
                    <p>Nome completo</p>
                    <input type="text" name="nome" placeholder="Digite aqui o seu nome" required hc-mail-message>
                    <p>Seu e-mail</p>
                    <input type="email" name="email" placeholder="Digite aqui o seu e-mail de contato" required hc-mail-message>
                    <p>Telefone</p>
                    <input id="telefone" type="text" name="telefone" placeholder="Digite aqui o seu telefone" required hc-mail-message>
                    <p>Mensagem</p>
                    <textarea name="mensagem" placeholder="Digite sua mensagem" hc-mail-message></textarea>
                    <button type="submit">Enviar E-mail</button>
                    <span data-hc-feedback></span>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container-valores">
    <div class="container-padrao">
        <?php 
            if(!empty(get_field('bloco_valores')['valores'])){
                $valores = get_field('bloco_valores')['valores'];
                $contadorValores = count($valores);
                for($j = 0; $j < $contadorValores; $j++){
                    $valor = $valores[$j];
        ?>  
            <div class="container-valor anime anime-left" style="transition-delay: .<?=$j?>s;">
                <div class="container-imagem">
                    <img src="<?=$valor['imagem']['url']?>">
                </div>
                <div class="container-conteudo">
                    <h1><?=$valor['titulo']?></h1>
                    <p><?=$valor['texto']?>
                        <?php 
                            if(!empty($valor['imagem_adicional']['url'])){
                        ?>
                            <img src="<?=$valor['imagem_adicional']['url']?>">
                        <?php } ?>
                    </p>
                </div>
            </div>
        <?php } } ?>
    </div>
</div>
<div class="container-localizacao-pai">
    <div class="container-padrao anime anime-top">
        <h1><?=get_field('bloco_localizacao')['titulo']?></h1>
        <div class="container-localizacao">
            <div class="container-horario">
                <div class="container-imagem">
                    <img src="<?=get_stylesheet_directory_uri()?>/img/clock-large.svg">
                </div>
                <div class="container-conteudo">
                    <p><?=get_field('bloco_localizacao')['horario_de_funcionamento']?></p>
                </div>
            </div>
            <?php 
                if(!empty(get_field('bloco_localizacao')['enderecos'])){
                    $enderecos = get_field('bloco_localizacao')['enderecos'];
                    $contadorEnderecos = count($enderecos);
                    $latitudeSede = '';
                    $longitudeSede = '';
                    $marcadores = array();
                    for($i = 0; $i < $contadorEnderecos; $i++){
                        $endereco = $enderecos[$i];
                        $marcadores[$i]['nome'] = $endereco['titulo'];
                        $marcadores[$i]['lat'] = $endereco['latitude'];
                        $marcadores[$i]['lon'] = $endereco['longitude'];
                        $marcadores[$i]['conjunto'] = $i;
                        if($i == 0){
                            $latitudeSede = $endereco['latitude'];
                            $longitudeSede = $endereco['longitude'];
                            $nomeSede = $endereco['titulo'];
                        }
            ?>  
                <div class="container-endereco anime anime-left" onclick="mapCenterController(<?=$i?>)" style="transition-delay: .<?=$i?>s;">
                    <div class="container-imagem">
                        <img src="<?=get_stylesheet_directory_uri()?>/img/map-marker.svg">
                    </div>
                    <div class="container-conteudo">
                        <p><?=$endereco['endereco']?></p>
                    </div>
                </div>
            <?php } } ?>
        </div>
    </div>
    <div class="map" id="map">

    </div>
</div>
<script>
    var map;
    var marcadores = <?= json_encode($marcadores); ?>;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: <?= $latitudeSede ?>, lng: <?= $longitudeSede ?>},
            zoom: 18,
            styles: [
                { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
                { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
                { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
                {
                    featureType: "administrative.locality",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#d59563" }]
                },
                {
                    featureType: "poi",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#d59563" }]
                },
                {
                    featureType: "poi.park",
                    elementType: "geometry",
                    stylers: [{ color: "#263c3f" }]
                },
                {
                    featureType: "poi.park",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#6b9a76" }]
                },
                {
                    featureType: "road",
                    elementType: "geometry",
                    stylers: [{ color: "#38414e" }]
                },
                {
                    featureType: "road",
                    elementType: "geometry.stroke",
                    stylers: [{ color: "#212a37" }]
                },
                {
                    featureType: "road",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#9ca5b3" }]
                },
                {
                    featureType: "road.highway",
                    elementType: "geometry",
                    stylers: [{ color: "#746855" }]
                },
                {
                    featureType: "road.highway",
                    elementType: "geometry.stroke",
                    stylers: [{ color: "#1f2835" }]
                },
                {
                    featureType: "road.highway",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#f3d19c" }]
                },
                {
                    featureType: "transit",
                    elementType: "geometry",
                    stylers: [{ color: "#2f3948" }]
                },
                {
                    featureType: "transit.station",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#d59563" }]
                },
                {
                    featureType: "water",
                    elementType: "geometry",
                    stylers: [{ color: "#17263c" }]
                },
                {
                    featureType: "water",
                    elementType: "labels.text.fill",
                    stylers: [{ color: "#515c6d" }]
                },
                {
                    featureType: "water",
                    elementType: "labels.text.stroke",
                    stylers: [{ color: "#17263c" }]
                }
            ]
        });

        var sede = {
            scaledSize: new google.maps.Size(20, 20),
            position: { lat: <?= $latitudeSede; ?> , lon: <?= $longitudeSede; ?>, nome: '<?= $nomeSede; ?>' },
        }

        adicionarMarcadores(marcadores);
    }
    function adicionarMarcadores(marcadores){
        let count = marcadores.length;
        for(var i = 0; i < count; i++){
            var marcador = marcadores[i];
            var latitude = parseFloat(marcador.lat);
            var longitude = parseFloat(marcador.lon);
            var longitude = parseFloat(marcador.lon);
            var titulo = marcador.nome;
            var conjunto = marcador.conjunto;
            var marker = new google.maps.Marker({
                position: {lat: latitude, lng: longitude},
                title: titulo,
                map: map,
                id: conjunto,
                scaledSize: marcador.scaledSize,
            });
        }
    }
    function mapCenterController(id){
        map.setCenter(new google.maps.LatLng(marcadores[id].lat, marcadores[id].lon));
        map.setZoom(18);
        abrirDropDown();
    }
    jQuery(document).ready(function($){ 
        $('#telefone').mask('(00) 00000-0000');
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAh-l_buF-q5g2C4ttkiQ5So9UjfAG0mSk&callback=initMap" async defer></script>
<?php get_footer(); ?>
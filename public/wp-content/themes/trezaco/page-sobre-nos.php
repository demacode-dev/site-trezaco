<?php
    wp_enqueue_style('css_sobre', get_stylesheet_directory_uri().'/src/css/sobre-nos.min.css', array(), null, false);
    get_header();
?>
    <div class="container-banner">
        <div class="container-background">
            <img src="<?=get_field('bloco_banner')['imagem_background']['url']?>">
            <div class="container-gradient"></div>
        </div>
        <div class="container-breadcrumb">
            <div class="container-padrao">
                <img class="home" src="<?php echo get_stylesheet_directory_uri()?>/img/home-solid.svg">
                <img class="arrow" src="<?php echo get_stylesheet_directory_uri()?>/img/orange-next-arrow.svg">
                <a href="/sobre-nos">Sobre nós</a>
            </div>
        </div>
        <div class="container-padrao">
            <div class="container-conteudo">
                <h1><?=get_field('bloco_banner')['titulo']?></h1>
            </div>
        </div>
    </div>
    <div class="container-valores-sobre">
        <div class="container-padrao">
            <?php 
                if(!empty(get_field('bloco_valores')['valores'])){
                    $valores = get_field('bloco_valores')['valores'];
                    $contadorValores = count($valores);
                    for($j = 0; $j < $contadorValores; $j++){
                        $valor = $valores[$j];
            ?>  
                <div class="container-valor anime anime-fade" style="transition-delay: .<?=$j?>s">
                    <div class="container-imagem">
                        <img src="<?=$valor['imagem']['url']?>">
                    </div>
                    <div class="container-conteudo">
                        <h1><?=$valor['titulo']?></h1>
                        <p><?=$valor['texto']?>
                            <?php 
                                if(!empty($valor['imagem_adicional']['url'])){
                            ?>
                                <img src="<?=$valor['imagem_adicional']['url']?>">
                            <?php } ?>
                        </p>
                    </div>
                </div>
            <?php } } ?>
        </div>
    </div>
    <div class="container-historia">
        <div class="container-background">
            <div class="container-bloco"></div>
        </div>
        <div class="container-padrao">
            <div class="container-conteudo-pai">
                <div class="container-conteudo anime anime-fade">
                    <h1><?=get_field('bloco_historia')['titulo']?></h1>
                    <p><?=get_field('bloco_historia')['texto']?></p>
                </div>
            </div>
            <img src="<?=get_field('bloco_historia')['imagem']['url']?>">
        </div>
    </div>
    <div class="container-idealizadores-pai">
        <div class="container-padrao anime anime-top">
            <h1 class="titulo"><?=get_field('bloco_idealizadores')['titulo']?></h1>
            <div class="container-idealizadores">
                <?php 
                    $postIdealizadores = array(
                        'post_type' => 'idealizadores',
                        'orderby' => 'name',
                        'post_status' => 'publish',
                        'order' => 'ASC',
                        'hide_empty' => false
                    );
                    $idealizadores = get_posts($postIdealizadores);
                    $contadorIdealizadores = count($idealizadores);
                    for($i = 0; $i < $contadorIdealizadores; $i++){
                        $idealizador = $idealizadores[$i];
                ?>
                    <div class="container-idealizador anime anime-fade" style="transition-delay: .<?=$i?>s">
                        <div class="container-conteudo">
                            <div class="container-imagem">
                                <img src="<?=get_field('imagem', $idealizador->ID)['url']?>">
                            </div>
                            <h1><?=get_field('nome', $idealizador->ID)?></h1>
                            <?=get_field('descricao', $idealizador->ID)?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="container-politica">
        <div class="container-background">
            <img src="<?=get_field('bloco_politica_de_privacidade')['imagem']['url']?>">
            <div class="container-bloco"></div>
        </div>
        <div class="container-padrao">
            <div class="container-conteudo-pai">
                <div class="container-conteudo anime anime-fade">
                    <h1 class="anime anime-fade"><?=get_field('bloco_politica_de_privacidade')['titulo']?></h1>
                    <p class="anime anime-fade"><?=get_field('bloco_politica_de_privacidade')['texto']?></p>
                    <div class="container-imagens">
                        <?php
                            $imagens = get_field('bloco_politica_de_privacidade')['imagens_normas'];
                            $contadorImagens = count($imagens);
                            for($i =0; $i < $contadorImagens; $i++){
                                $imagem = $imagens[$i];
                        ?>
                            <div class="container-imagem anime anime-fade" style="transition-delay: .<?=$i?>s">
                                <img src="<?=$imagem['imagem']['url']?>">
                            </div>
                        <?php } ?>
                    </div>
                    <h2 class="anime anime-fade"><?=get_field('bloco_politica_de_privacidade')['subtitulo']?></h2>
                    <div class="container-link anime anime-fade">
                        <a href="/normas">Ver normas</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-linha-produtos">
        <div class="container-padrao anime anime-top">
            <h1 class="titulo"><?=get_field('bloco_linha_de_produtos')['titulo']?></h1>
            <div class="container-linha">
                <?php 
                    $argsProdutos = array(
                        'post_type'  => 'produtos',
                        'taxonomy' => 'categoria_produto',
                        'parent' => '0',
                        'hide_empty' => false,
                        'orderby'	=> 'meta_value',
                        'order' => "ASC",
                        'meta_key' => 'ordem_categoria'
                    );  
                    $produtosCategorias = get_terms($argsProdutos);
                    $contadorCategorias = count($produtosCategorias);
                    for($i = 0; $i < $contadorCategorias; $i++){
                        $categoriaPai = $produtosCategorias[$i];
                        if($categoriaPai->slug == 'conexoes' || $categoriaPai->slug == 'valvulas'){
                        }else{
                ?>
                    <a href="/produtos/categoria/<?=$categoriaPai->slug?>">
                        <div class="container-categoria anime anime-fade" style="transition-delay: .<?=$i?>s">
                            <div class="container-imagem">
                                <img src="<?=!empty(get_field('imagem_em_destaque', $categoriaPai)['url']) ? get_field('imagem_em_destaque', $categoriaPai)['url'] : get_stylesheet_directory_uri().'/img/iron-bar.svg'?>">
                            </div>
                            <h1><?=$categoriaPai->name?></h1>
                        </div>
                    </a>
                <?php } }?>
            </div>
        </div>
    </div>
<script>
    jQuery(document).ready(function($){ 
    });
</script>
<?php get_footer(); ?>
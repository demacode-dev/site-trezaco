		<div class="container-banner-normas">
			<div class="background"></div>
			<div class="container-padrao">
				<div class="container-botao">
					<h1><?=get_field('bloco_banner_normas', 'footer')['titulo']?></h1>
					<div class="botao">
						<a href="/normas">Saiba mais</a>
					</div>
				</div>
				<div class="container-selos">
					<?php
						$imagens = get_field('bloco_banner_normas', 'footer')['imagens_logo'];
						$contadorImagens = count($imagens);
						for($i =0; $i < $contadorImagens; $i++){
							$imagem = $imagens[$i];
					?>
						<div class="container-imagem">
							<img src="<?=$imagem['imagem']['url']?>">
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</main>

</div>
	<footer class="footer">
		<a href="https://api.whatsapp.com/send?phone=5541988435253&text=Olá,%20gostaria%20de%20um%20orçamento!" class="bt-whatsApp" target="_blank" style="left:25px; position:fixed;width:60px;height:60px;bottom:40px;z-index:100;">
			<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI2MHB4IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA2MCA2MCIgd2lkdGg9IjYwcHgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6c2tldGNoPSJodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2gvbnMiIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48dGl0bGUvPjxkZWZzLz48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGlkPSJmbGF0IiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSI+PGcgaWQ9IndoYXRzYXBwIj48cGF0aCBkPSJNMzAsNjAgQzQ2LjU2ODU0MzMsNjAgNjAsNDYuNTY4NTQzMyA2MCwzMCBDNjAsMTMuNDMxNDU2NyA0Ni41Njg1NDMzLDAgMzAsMCBDMTMuNDMxNDU2NywwIDAsMTMuNDMxNDU2NyAwLDMwIEMwLDQ2LjU2ODU0MzMgMTMuNDMxNDU2Nyw2MCAzMCw2MCBaIiBmaWxsPSIjNTdCQjYzIi8+PHBhdGggZD0iTTMwLjA3MTI2MTUsNDYuMjIxMDQ2MiBDMjcuMjEwODMwOCw0Ni4yMjEwNDYyIDI0LjUyMzU2OTIsNDUuNDg5OTY5MiAyMi4xODU2LDQ0LjIwNjg5MjMgTDEzLjE1Mzg0NjIsNDcuMDc2OTIzMSBMMTYuMDk4MDkyMywzOC4zOTE4NzY5IEMxNC42MTMwNDYyLDM1Ljk1MjM2OTIgMTMuNzU3NTM4NSwzMy4wOTE1NjkyIDEzLjc1NzUzODUsMzAuMDMzNiBDMTMuNzU3NTM4NSwyMS4wOTM0MTU0IDIxLjA2MTI5MjMsMTMuODQ2MTUzOCAzMC4wNzE2MzA4LDEzLjg0NjE1MzggQzM5LjA4MDg2MTUsMTMuODQ2MTUzOCA0Ni4zODQ2MTU0LDIxLjA5MzQxNTQgNDYuMzg0NjE1NCwzMC4wMzM2IEM0Ni4zODQ2MTU0LDM4Ljk3Mzc4NDYgMzkuMDgxMjMwOCw0Ni4yMjEwNDYyIDMwLjA3MTI2MTUsNDYuMjIxMDQ2MiBaIE0zMC4wNzEyNjE1LDE2LjQyNDEyMzEgQzIyLjUwNzkzODUsMTYuNDI0MTIzMSAxNi4zNTU4MTU0LDIyLjUyOTM1MzggMTYuMzU1ODE1NCwzMC4wMzM2IEMxNi4zNTU4MTU0LDMzLjAxMTQ0NjIgMTcuMzI2NTIzMSwzNS43NjkyMzA4IDE4Ljk2ODEyMzEsMzguMDEzMDQ2MiBMMTcuMjU0ODkyMyw0My4wNjcwNzY5IEwyMi41MjUyOTIzLDQxLjM5MTg3NjkgQzI0LjY5MTIsNDIuODEzNzg0NiAyNy4yODU0MTU0LDQzLjY0MzA3NjkgMzAuMDcxMjYxNSw0My42NDMwNzY5IEMzNy42MzM0NzY5LDQzLjY0MzA3NjkgNDMuNzg2NzA3NywzNy41MzgyMTU0IDQzLjc4NjcwNzcsMzAuMDMzOTY5MiBDNDMuNzg2NzA3NywyMi41Mjk3MjMxIDM3LjYzMzQ3NjksMTYuNDI0MTIzMSAzMC4wNzEyNjE1LDE2LjQyNDEyMzEgTDMwLjA3MTI2MTUsMTYuNDI0MTIzMSBaIE0zOC4zMDg4LDMzLjc2MTcyMzEgQzM4LjIwODM2OTIsMzMuNTk2Njc2OSAzNy45NDE3ODQ2LDMzLjQ5Njk4NDYgMzcuNTQyNjQ2MiwzMy4yOTg3MDc3IEMzNy4xNDI0LDMzLjEwMDQzMDggMzUuMTc1ODc2OSwzMi4xNDAwNjE1IDM0LjgwOTk2OTIsMzIuMDA4MjQ2MiBDMzQuNDQyOTUzOCwzMS44NzYwNjE1IDM0LjE3NiwzMS44MDkyMzA4IDMzLjkwOTc4NDYsMzIuMjA2NTIzMSBDMzMuNjQzNTY5MiwzMi42MDM4MTU0IDMyLjg3NzA0NjIsMzMuNDk2OTg0NiAzMi42NDMzMjMxLDMzLjc2MTcyMzEgQzMyLjQwOTk2OTIsMzQuMDI2ODMwOCAzMi4xNzY5ODQ2LDM0LjA2MDA2MTUgMzEuNzc3MTA3NywzMy44NjE0MTU0IEMzMS4zNzc2LDMzLjY2MzEzODUgMzAuMDg4OTg0NiwzMy4yNDQwNjE1IDI4LjU2MTEwNzcsMzEuODkyMzA3NyBDMjcuMzcyNTUzOCwzMC44NDA3Mzg1IDI2LjU2OTg0NjIsMjkuNTQyNTIzMSAyNi4zMzY4NjE1LDI5LjE0NDg2MTUgQzI2LjEwMzUwNzcsMjguNzQ3OTM4NSAyNi4zMTIxMjMxLDI4LjUzMzQxNTQgMjYuNTEyMjQ2MiwyOC4zMzU4NzY5IEMyNi42OTIwNjE1LDI4LjE1NzkwNzcgMjYuOTEyMTIzMSwyNy44NzI0OTIzIDI3LjExMjI0NjIsMjcuNjQwOTg0NiBDMjcuMzEyMzY5MiwyNy40MDkxMDc3IDI3LjM3ODgzMDgsMjcuMjQ0MDYxNSAyNy41MTE3NTM4LDI2Ljk3ODk1MzggQzI3LjY0NTQxNTQsMjYuNzE0MjE1NCAyNy41Nzg1ODQ2LDI2LjQ4MjcwNzcgMjcuNDc4NTIzMSwyNi4yODM2OTIzIEMyNy4zNzg0NjE1LDI2LjA4NTQxNTQgMjYuNTc4MzM4NSwyNC4xMzI5MjMxIDI2LjI0NTI5MjMsMjMuMzM4MzM4NSBDMjUuOTEyMjQ2MiwyMi41NDQ0OTIzIDI1LjU3OTU2OTIsMjIuNjc2Njc2OSAyNS4zNDU4NDYyLDIyLjY3NjY3NjkgQzI1LjExMjQ5MjMsMjIuNjc2Njc2OSAyNC44NDU5MDc3LDIyLjY0MzQ0NjIgMjQuNTc5MzIzMSwyMi42NDM0NDYyIEMyNC4zMTI3Mzg1LDIyLjY0MzQ0NjIgMjMuODc5MjYxNSwyMi43NDI3NjkyIDIzLjUxMjYxNTQsMjMuMTM5NjkyMyBDMjMuMTQ2MzM4NSwyMy41MzY5ODQ2IDIyLjExMzYsMjQuNDk2OTg0NiAyMi4xMTM2LDI2LjQ0OTEwNzcgQzIyLjExMzYsMjguNDAxNiAyMy41NDU4NDYyLDMwLjI4OCAyMy43NDYzMzg1LDMwLjU1MjM2OTIgQzIzLjk0NjA5MjMsMzAuODE2NzM4NSAyNi41MTE4NzY5LDM0Ljk1MzYgMzAuNTc2NzM4NSwzNi41NDI0IEMzNC42NDMwNzY5LDM4LjEzMDgzMDggMzQuNjQzMDc2OSwzNy42MDA5ODQ2IDM1LjM3NjM2OTIsMzcuNTM0ODkyMyBDMzYuMTA4NTUzOCwzNy40Njg4IDM3Ljc0MTI5MjMsMzYuNTc1MjYxNSAzOC4wNzU0NDYyLDM1LjY0ODg2MTUgQzM4LjQwODEyMzEsMzQuNzIxNzIzMSAzOC40MDgxMjMxLDMzLjkyNzEzODUgMzguMzA4OCwzMy43NjE3MjMxIEwzOC4zMDg4LDMzLjc2MTcyMzEgWiIgZmlsbD0iI0ZGRkZGRiIvPjwvZz48L2c+PC9zdmc+" alt="" width="60px">
		</a>
		<span id="alertWapp" style="left:30px; visibility: hidden; position:fixed;	width:17px;	height:17px;bottom:90px; background:red;z-index:101; font-size:11px;color:#fff;text-align:center;border-radius: 50px; font-weight:bold;line-height: normal; "> 1 </span>
		<div id="msg1" style="left: 90px; visibility: hidden; background: #fff; color: #000;position: fixed;width: 180px;height: fit-content; bottom: 75px;font-size: 16px;line-height: 16px;padding: 10px; border-radius: 10px; border:1px solid #e2e2e2; box-shadow: 2px 2px 3px #999;z-index:100; text-align: center; font-family: Arial, Helvetica, sans-serif; ">Estamos aqui também!</div>
		<div class="container-padrao">
			<h2 onclick="scrollParaElemento('body')">Voltar para o topo</h2>
			<div class="container-pages">
				<ul>
					<li><a href="/">Home</a></li>
					<li><a href="/produtos">Produtos</a></li>
					<li><a href="/sobre-nos">Sobre nós</a></li>
					<li><a href="/normas">Normas</a></li>
					<li><a href="/blog">Blog</a></li>
					<li><a href="/contato">Contato</a></li>
				</ul>
			</div>
			<div class="container-info-pai">
				<div class="container-info">
					<div class="container-contato">
						<h1><?=get_field('bloco_contato', 'footer')['titulo']?></h1>
						<p><?=get_field('bloco_contato', 'footer')['telefone']?></p>
						<p><?=get_field('bloco_contato', 'footer')['email']?></p>
						<p><?=get_field('bloco_contato', 'footer')['endereco']?></p>
					</div>
					<div class="container-redes-pagamento">
						<div class="container-redes-pai">
							<h1><?=get_field('bloco_redes_sociais', 'footer')['titulo']?></h1>
							<div class="container-redes">
								<?php
									$redes = get_field('bloco_redes_sociais', 'footer')['redes_sociais'];
									$contadorRedes = count($redes);
									for($i =0; $i < $contadorRedes; $i++){
										$rede = $redes[$i];
								?>
									<a target="_blank" href="<?= $rede['link']?>"><img src="<?= $rede['icone']['url']?>"></a>
								<?php } ?>
							</div>
						</div>
						<div class="container-pagamento">
							<h1><?=get_field('bloco_pagamentos', 'footer')['titulo']?></h1>
							<p><?=get_field('bloco_pagamentos', 'footer')['texto']?><img src="<?=get_field('bloco_pagamentos', 'footer')['imagem_complementar']['url']?>"></p>
						</div>
					</div>
				</div>
				<div class="container-boleto">
					<img src="<?=get_field('logo_empresa', 'footer')['url']?>">
					<a href="<?=get_field('link_botao', 'footer')?>">
						<div class="container-segunda-via">
							<p>Segunda via de boleto</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="container-direitos-pai">
			<div class="container-padrao">
				<p><?=get_field('texto', 'footer')?></p>
				<p>Desenvolvido por: <img src="<?=get_stylesheet_directory_uri()?>/img/padrao/dema_logo.png"></p>
			</div>
		</div>
		<script type="text/javascript">
			function showIt2() {
				document.getElementById("msg1").style.visibility = "visible";
			}    
			setTimeout("showIt2()", 5000); 

			function hiddenIt() {
				document.getElementById("msg1").style.visibility = "hidden";
			}
			setTimeout("hiddenIt()", 15000); 

			function showIt3() {
				document.getElementById("msg1").style.visibility = "visible";
			} 
			setTimeout("showIt3()", 25000);  

			msg1.onclick = function() {
				document.getElementById('msg1').style.visibility = "hidden"; 
			};
			function alertW() { 
				document.getElementById("alertWapp").style.visibility = "visible";	
			} 
			setTimeout("alertW()", 15000); 
		</script>
	</footer>
    <?php wp_footer();?>
</body>
</html>

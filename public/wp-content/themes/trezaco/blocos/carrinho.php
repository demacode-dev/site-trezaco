<?php 
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    
    if(!empty($_SESSION["carrinho"])){
        $carrinhoProdutosEscolhidos = $_SESSION["carrinho"];
        $carrinhoQuantidadeProdutosEscolhidos = count($carrinhoProdutosEscolhidos);
        $carrinhoProdutosJson = json_encode($carrinhoProdutosEscolhidos);
    }else{
        $carrinhoProdutosEscolhidos = '';
        $carrinhoQuantidadeProdutosEscolhidos = 0;
        $carrinhoProdutosJson = '';
    }
?>
<div class="container-conteudo">
    <?php if($carrinhoQuantidadeProdutosEscolhidos == 0){ ?>
        <p>Seu carrinho está vazio</p>
    <?php 
        }else{ 
            for($i = 0; $i < $carrinhoQuantidadeProdutosEscolhidos; $i++){
                $produtoCarrinho = (object) $carrinhoProdutosEscolhidos[$i];
                $nomeCarrinho = $produtoCarrinho->nome;
                $referenciaCarrinho = $produtoCarrinho->referencia;
                $imagemCarrinho = $produtoCarrinho->imagem_url;
                $quantidadeCarrinho = $produtoCarrinho->quantidade;
    ?>
        <div class="container-produto linha-produto" id="linha-produto-<?=$i?>">
            <div class="container-info-pai">
                <div class="container-imagem">
                    <img src="<?=$imagemCarrinho?>">
                </div>
                <div class="container-info">
                    <h1><?=$nomeCarrinho?></h1>
                    <div class="container-quantidade-pai">
                        <div class="container-quantidade">
                            <p>Qtd</p>
                            <div class="quantidade-produto">
                                <button onclick="aumentarDiminuirQuantidadeProdutoA('<?= $i ?>', '-')">
                                    -
                                </button>
                                <input type="number" class="quantidade-produto-input" id="quantidade-produto-<?= $i ?>" readonly="readonly" value="<?=$quantidadeCarrinho?>">
                                <button onclick="aumentarDiminuirQuantidadeProdutoA('<?= $i ?>', '+')">
                                    +
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-remove">
                <div class="container-remove-image" onclick="cancelarProduto('<?= $i ?>')">
                    <img src="<?=get_stylesheet_directory_uri()?>/img/lixeira.svg">
                </div>
            </div>
        </div>
    <?php } }?>
</div>
<?php
    wp_enqueue_style('css_single-blog', get_stylesheet_directory_uri().'/src/css/single-blog.min.css', array(), null, false);
    get_header();

    $dataHora = $post->post_date;
    $dataHoraSplit = explode(' ', $dataHora);
    $data = explode('-', $dataHoraSplit[0]);
    $dataAno = $data[0];
    $dataMes = $data[1]; 
    $dataDia = $data[2];
    $listaMeses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
?>
<div class="container-breadcrumb">
    <div class="container-padrao">
        <img class="home" src="<?php echo get_stylesheet_directory_uri()?>/img/home-solid.svg">
        <img class="arrow" src="<?php echo get_stylesheet_directory_uri()?>/img/orange-next-arrow.svg">
        <a href="/blog">Blog</a>
    </div>
</div>
<div class="container-post-blog-pai">
    <div class="container-padrao">
        <div class="container-titulo">
            <h1><?=get_field('titulo', $post)?></h1>
            <p><?=get_field('texto', $post)?></p>
        </div>
        <div class="container-post-blog">
            <div class="container-post-conteudo">
                <div class="container-autor">
                    <div class="container-imagem-autor">
                        <img src="<?= !empty(get_field('imagem_do_autor', $post)['url']) ? get_field('imagem_do_autor', $post)['url'] : get_stylesheet_directory_uri().'/img/perfil-desc.jpg'?>">
                    </div>
                    <div class="container-info">
                        <p><?= !empty(get_field('nome_do_autor', $post)) ? get_field('nome_do_autor', $post) : 'Desconhecido'?></p>
                        <p><?=$listaMeses[((int)$dataMes - 1)]?> <?=$dataDia?>, <?=$dataAno?></p>
                    </div>
                </div>
                <div class="container-imagem">
                    <img src="<?=get_field('imagem_destaque', $post)['url']?>">
                </div>
                <div class="texto-completo">
                    <?=get_field('texto_completo', $post)?>
                </div>
            </div>
            <div class="container-categorias-relacionados">
                <div class="container-categorias">
                    <h1>Categorias</h1>
                    <div class="container-tags">
                        <?php 
                            $argsBlogTags = array(
                                'post_type'  => 'blog',
                                'taxonomy' => 'categoria_blog',
                                'orderby' => 'name',
                                'order'   => 'ASC',
                                'parent' => '0',
                                'hide_empty' => false
                            );  
                            $blogTags = get_terms($argsBlogTags);

                            if(!empty($blogTags)){
                                $contadorTags = count($blogTags);
                                for($j = 0; $j < $contadorTags; $j++){
                                    $tag = $blogTags[$j];
                        ?>
                            <a href="/blog/categoria/<?=$tag->slug?>"> <?=$tag->name?> </a>
                        <?php } } ?>
                    </div>
                </div>
                <div class="container-posts-relacionados">
                    <h1 class="titulo-relacionados">Mais artigos da mesma categoria</h1>
                    <div class="container-posts">
                        <?php 
                            $categoriaRelacionado = get_the_terms($post, 'categoria_blog');

                            $taxQueryRelacionado =  array(
                                array(
                                    'taxonomy' => 'categoria_blog',
                                    'field' => 'slug',
                                    'terms' => $categoriaRelacionado[0]->slug
                                )
                            );

                            $postsBlogRelacionado = array(
                                'post_type' => 'blog',
                                'orderby' => 'name',
                                'post_status' => 'publish',
                                'order' => 'ASC',
                                'hide_empty' => false,
                                'posts_per_page' => 3,
                                'post__not_in' => array (get_the_ID($post)),
                                'numberposts' => 3,
                                'tax_query' => $taxQueryRelacionado,
                            );
                            $blogRelacionado = get_posts($postsBlogRelacionado);
                            if(!empty($blogRelacionado)){
                                $contadorBlogRelacionado = count($blogRelacionado);
                                for($i = 0; $i < $contadorBlogRelacionado; $i++){
                                    $postBlogRelacionado = $blogRelacionado[$i];
                        ?>
                            <div class="container-post">
                                <div class="container-imagem">
                                    <img src="<?=get_field('imagem_destaque', $postBlogRelacionado->ID)['url']?>">
                                    <div class="container-tag">
                                        <p></p>
                                    </div>
                                </div>
                                <h1>
                                    <?php 
                                        if(strlen(get_field('titulo', $postBlogRelacionado->ID)) > 20){
                                            echo substr(get_field('titulo', $postBlogRelacionado->ID),0, 17)."..."; 
                                        }else{
                                            echo get_field('titulo', $postBlogRelacionado->ID); 	
                                        }
                                    ?>
                                </h1>
                                <p>
                                    <?php 
                                        if(strlen(get_field('texto', $postBlogRelacionado->ID)) > 45){
                                            echo substr(get_field('texto', $postBlogRelacionado->ID),0, 42)."..."; 
                                        }else{
                                            echo get_field('texto', $postBlogRelacionado->ID); 	
                                        }
                                    ?>
                                </p>
                                <div class="container-ler-mais">
                                    <a href="/blog/<?= $postBlogRelacionado->post_name?>">Ler postagem</a>
                                </div>
                            </div>
                        <?php } }else{ ?>
                            <h1>Não há posts relacionados</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-comentarios">
            <h1>Comentários</h1>
            <div id="disqus_thread"></div>
            <script>
                /**
                *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                var disqus_config = function () {
                    this.page.url = '<?=get_permalink()?>';  // Replace PAGE_URL with your page's canonical URL variable
                    this.page.identifier = '<?=get_permalink()?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                };
                (function() { // DON'T EDIT BELOW THIS LINE
                    var d = document, s = d.createElement('script');
                    s.src = 'https://trezaco.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                
        </div>
    </div>
</div>
<?php get_footer();?>
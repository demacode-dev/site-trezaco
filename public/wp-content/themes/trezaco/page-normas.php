<?php
    wp_enqueue_style('css_normas', get_stylesheet_directory_uri().'/src/css/normas.min.css', array(), null, false);
    get_header();
?>
<div class="container-normas-pai">
    <div class="container-background-overlayer">
        <img class="imagem-background" src="<?=get_field('bloco_normas')['imagem_background']['url']?>">
        <div class="overlayer"></div>
        <img class="imagem-topo" src="<?=get_field('bloco_normas')['imagem_topo']['url']?>">
    </div>
    <div class="container-breadcrumb">
        <div class="container-padrao">
            <img class="home" src="<?php echo get_stylesheet_directory_uri()?>/img/home-solid.svg">
            <img class="arrow" src="<?php echo get_stylesheet_directory_uri()?>/img/orange-next-arrow.svg">
            <a href="/normas">Normas</a>
        </div>
    </div>
    <div class="container-padrao">
        <h1 class="anime anime-fade"><?=get_field('bloco_normas')['titulo']?></h1>
        <div class="container-normas">
            <?php 
                $argsCategoriasNormas = array(
                    'post_type'  => 'normas',
                    'taxonomy' => 'categoria_norma',
                    'orderby' => 'name',
                    'order'   => 'ASC',
                    'parent' => '0',
                    'hide_empty' => false
                );  
                $categoriasNormas = get_terms($argsCategoriasNormas);

                $contadorCategoriasNormas = count($categoriasNormas);
                for($i = 0; $i < $contadorCategoriasNormas; $i++){
                    $norma = $categoriasNormas[$i];
            ?>
                <div class="container-norma-pai anime anime-left">
                    <div class="container-norma">
                        <div class="container-imagem norma-control norma-<?=$i?>">
                            <img src=" <?=get_field('logo', $norma)['url'] ?>">
                        </div>
                        <div class="container-descricao">
                            <h1 class="norma-control norma-<?=$i?>"><?= get_field('titulo', $norma) ?></h1>
                            <p class="norma-control norma-<?=$i?>"><?= wp_strip_all_tags($norma->description) ?></p>
                        </div>
                    </div>
                    <div class="container-conteudo-card-pai conteudo-card-<?=$i?>">
                        <div class="container-conteudo">
                            <div class="container-cards">
                                <?php 
                                    $argsCardsNormas = array(
                                        'post_type' => 'normas',
                                        'orderby' => 'name',
                                        'post_status' => 'publish',
                                        'order' => 'ASC',
                                        'hide_empty' => false,
                                        'posts_per_page' => -1,
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => 'categoria_norma',
                                                'field' => 'slug',
                                                'terms' => array($norma->slug)
                                            )
                                        )
                                    );
                                    $cardsNormas = get_posts($argsCardsNormas);
                                    if(!empty($cardsNormas)){
                                        $contadorCardsNormas = count($cardsNormas);
                                        for($j = 0; $j < $contadorCardsNormas; $j++){
                                            $cardNorma = $cardsNormas[$j];
                                ?>
                                    <div class="container-card-pai">
                                        <div class="container-card">
                                            <img src="<?=get_field('imagem', $cardNorma->ID)['url']?>">
                                            <h1><?=get_field('titulo', $cardNorma->ID)?></h1>
                                            <p><?=get_field('descricao', $cardNorma->ID)?></p>
                                        </div>
                                    </div>
                                <?php } 
                                    } else{ 
                                ?>
                                    <div class="container-vazio">
                                        <p>Nenhum processo de fabricação foi adicionado à está norma</p>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="container-botao-control norma-control norma-<?=$i?>" onclick="normasController('<?=$i?>')">
                        <p class="texto-controller-<?=$i?> inativo">Ver todos os processos de fabricação</p>
                        <img class="imagem-controller-<?=$i?> inativo" src="<?=get_stylesheet_directory_uri()?>/img/arrow-up.svg">
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php 
    if(!get_field('bloco_segmentos')['segmentos_checkbox'] == true){
?>
    <div class="container-segmentos">
        <div class="container-padrao anime anime-top">
            <h1 class="titulo"><?=get_field('bloco_segmentos')['titulo']?></h1>
            <div class="container-carrossel-pai">
                <div class="container-carrossel">
                    <div class="prev-arrow-segmentos">
                        <img src="<?=get_stylesheet_directory_uri()?>/img/orange-prev-arrow.svg">
                    </div>
                    <div class="container-carrossel-nav">
                        
                        <?php 
                            $segmentosArgs = array(
                                'post_type' => 'segmentos',
                                'orderby' => 'name',
                                'post_status' => 'publish',
                                'order' => 'ASC',
                                'hide_empty' => false,
                                'posts_per_page' => -1
                            );
                            $segmentos = get_posts($segmentosArgs);
                            $contadorSegmentos = count($segmentos);
                            for($i = 0; $i < $contadorSegmentos; $i++){
                                $segmento = $segmentos[$i];
                        ?>
                            <div class="container-segmento-nav-pai">
                                <div class="container-segmento-nav" style="background-image: url('<?= get_field('imagem_destaque', $segmento->ID)['url']?>')"> </div>
                                <h1><?=get_field('titulo', $segmento->ID)?></h1>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="next-arrow-segmentos">
                        <img src="<?=get_stylesheet_directory_uri()?>/img/orange-next-arrow.svg">
                    </div>
                    <div class="container-carrossel-content">
                        <?php
                            for($i = 0; $i < $contadorSegmentos; $i++){
                                $segmento = $segmentos[$i];
                        ?>
                            <div class="container-segmento-content">
                                <p><?= get_field('texto', $segmento->ID)?></p>
                                <div class="container-tags">
                                    <?php 
                                        if(!empty(get_the_tags($segmento->ID))){
                                            $tags = get_the_tags($segmento->ID);
                                            $contadorTags = count($tags);
                                            for($j = 0; $j < $contadorTags; $j++){
                                                $tag = $tags[$j];
                                    ?>
                                        <div class="container-tag">
                                            <p><?= $tag->name ?></p>
                                        </div>
                                    <?php } } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<div class="container-produtos-pai">
    <div class="container-padrao anime anime-top">
        <h1 class="titulo"><?=get_field('bloco_produtos')['titulo']?></h1>
        <div class="container-produtos">
            <?php 
                $argsCategorias = array(
                    'post_type'  => 'produtos',
                    'taxonomy' => 'categoria_produto',
                    'orderby' => 'name',
                    'order'   => 'ASC',
                    'parent' => '0',
                    'hide_empty' => false
                );  
                $produtosCategorias = get_terms($argsCategorias);

                $contadorCategorias = count($produtosCategorias);
                for($i = 0; $i < $contadorCategorias; $i++){
                    $categoriaPai = $produtosCategorias[$i];
                    if($categoriaPai->slug == 'conexoes' || $categoriaPai->slug == 'valvulas'){
                    }else{
            ?>
                <div class="container-produto anime anime-fade <?= ((($contadorCategorias % 2) == 1) && ($i == ($contadorCategorias - 1))) ? 'ultimo' : ''?>">
                    <div class="container-conteudo-pai">
                        <div class="container-background-botao">
                            <img src="<?=get_field('imagem_background', $categoriaPai)['url']?>">
                            <div class="overlayer"></div>
                            <div class="botao">
                                <a href="/produtos/categoria/<?=$categoriaPai->slug?>">Ver <?=$categoriaPai->name?></a>
                            </div>
                        </div>
                        <div class="container-conteudo">
                            <h1><?=get_field('titulo', $categoriaPai)?></h1>
                            <p><?=$categoriaPai->description?></p>
                        </div>
                    </div>
                </div>
            <?php } }?>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function($){
        jQuery('.container-conteudo-card-pai').slideToggle();
        $('.container-carrossel-content').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.container-carrossel-nav'
        });
        $('.container-carrossel-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.container-carrossel-content',
            centerMode: true,
            focusOnSelect: true,
            dots: false,
            arrows: true,
            prevArrow: $('.prev-arrow-segmentos'),
            nextArrow: $('.next-arrow-segmentos'),
            responsive: [
                {
                breakpoint: 550,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }
            ]
        });
    });
</script>
<?php get_footer(); ?>
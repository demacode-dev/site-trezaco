var page = 2;
var url = window.origin + '/wp-admin/admin-ajax.php';
var button = jQuery('.carregar-mais');
var carrinho = [];

jQuery(document).ready(function($){
	$('#container-loading').fadeOut();
	$('#body').removeClass('scroll-block');

    $target = $('.anime'),
	animationClass = 'anime-init',
	windowHeight = $(window).height(),
	offset = windowHeight - (windowHeight / 5);
	var root = document.documentElement;
	root.className += ' js';

	function boxTop(idBox) {
		var boxOffset = $(idBox).offset().top;
		return boxOffset;
	}

	function animeScroll() {
		var documentTop = $(document).scrollTop();
		$target.each(function() {
		if (documentTop > boxTop(this) - offset) {
			$(this).addClass(animationClass);
		}
		});
	}
	animeScroll();

	$(document).scroll(function() {
		$target = $('.anime');
		animeScroll();
	});
});

function normasController(id){
	jQuery('.norma-control').not('.norma-' + id).slideToggle();
	jQuery('.conteudo-card-' + id).slideToggle();
	if(jQuery('.texto-controller-' + id).hasClass('inativo')){
		jQuery('.texto-controller-' + id).text('Ocultar todos os processos de fabricação');
		jQuery('.texto-controller-' + id).removeClass('inativo');
		jQuery('.texto-controller-' + id).addClass('ativo');
		jQuery('.imagem-controller-' + id).removeClass('inativo');
		jQuery('.imagem-controller-' + id).addClass('ativo');
	}else{
		jQuery('.texto-controller-' + id).text('Ver todos os processos de fabricação');
		jQuery('.texto-controller-' + id).removeClass('ativo');
		jQuery('.texto-controller-' + id).addClass('inativo');
		jQuery('.imagem-controller-' + id).removeClass('ativo');
		jQuery('.imagem-controller-' + id).addClass('inativo');
	}
}

function produtosController(id){
	jQuery('.texto-controller').removeClass('ativo');
	jQuery('.texto-controller-' + id).addClass('ativo');
	jQuery('.container-categorias-filhas-slide').removeClass('ativo');
	jQuery('.texto-controller').removeClass('ativo');
	jQuery('.categorias-filhas-' + id).addClass('ativo');
	jQuery('.texto-controller-' + id).addClass('ativo');
}

function faqController(id){
	jQuery('.faq-controller').removeClass('ativo');
	jQuery('.faq-controller-' + id).addClass('ativo');
	jQuery('.container-faq-conteudo').fadeOut(200);
    setTimeout(function(){
        jQuery('.faq-conteudo-' + id).fadeIn(200);
    }, 200);
}

function scrollParaElemento(elemento){
	document.getElementById(elemento).scrollIntoView();
}
function abrirDropDown(id, medida){
	dropdown = document.getElementById('container-options-' + id);
	seta = document.getElementById('seta-' + id);
	if(dropdown.classList.contains('dropdown-ativo')){
		dropdown.classList.remove('dropdown-ativo');
		seta.classList.remove('dropdown-ativo');
	}else{
		dropdown.classList.add('dropdown-ativo');
		seta.classList.add('dropdown-ativo');
	}
}

function abrirDropDownFilha(idProduto, medida, idMedida){
	dropdown = document.getElementById('container-options-' + idProduto + '-' + medida + '-' + idMedida);
	seta = document.getElementById('seta-' + idProduto + '-' + medida + '-' + idMedida);
	if(dropdown.classList.contains('dropdown-ativo')){
		dropdown.classList.remove('dropdown-ativo');
		seta.classList.remove('dropdown-ativo');
	}else{
		dropdown.classList.add('dropdown-ativo');
		seta.classList.add('dropdown-ativo');
	}
}

function medidaValorController(valor, medida, idProduto, idMedida){
	document.getElementById('texto-selected-filha-' + medida + '-' + idProduto + '-' + idMedida).innerHTML = valor;
	abrirDropDownFilha(idProduto, medida, idMedida)
}

function medidasFilhaEspecialController(valor, idProduto){
	document.getElementById('texto-selected-filha-' + idProduto).innerHTML = valor;
}

function medidasController(valor ,idMedida, idProduto){
	jQuery('.container-medidas-' + idProduto).hide();
	jQuery('.container-medidas-' + idProduto + '-' + idMedida).show();
	jQuery('.texto-selected-filha-' + idProduto).removeClass('ativo');
	jQuery('.texto-selected-filha-' + idProduto + '-' + idMedida).addClass('ativo');
	document.getElementById('texto-selected-' + idProduto).innerHTML = valor;
	abrirDropDown(idProduto);
}
function medidasEspecialController(valor, idProduto){
	document.getElementById('texto-selected-' + idProduto).innerHTML = valor;
}

function abrirMenu(){
	var menuLateral = document.getElementById('menu-lateral');

	if(!menuLateral.classList.contains('mostrar-menu')) {
		menuLateral.classList.add('mostrar-menu');
			
	} else {
		menuLateral.classList.remove('mostrar-menu');
	}
}

function pesquisaController(){
	var inputBusca = document.getElementById('input-busca');
	if(inputBusca.value == ""){
		if(!inputBusca.classList.contains('ativo')) {
			inputBusca.classList.add('ativo');
		} else {
			inputBusca.classList.remove('ativo');
		}
	}else{
		var urlAtual = window.location.origin;
		
		window.location = urlAtual + "/blog/pesquisa/" + inputBusca.value;
	}
}
function aumentarDiminuirQuantidadeProduto(tipoOperacao, id) {
	var input = document.getElementById('quantidade-' + id);
	var quantidadeInput = input.value;
	if(tipoOperacao == '-') {
		if(quantidadeInput != 1) {
			quantidadeInput--
			input.value = quantidadeInput;
		}
		if(quantidadeInput == 1) {
			quantidadeInput==1
			input.value = quantidadeInput;
		}
	}
	if(tipoOperacao == '+') {
		quantidadeInput++
		input.value = quantidadeInput;
	}
}

function adicionarAoOrcamento(codigoRef, tituloProduto, imagemUrl, titulosArray, idProduto, quantidadeProduto, admin_url){
	valoresFilhas = document.querySelectorAll('.texto-selected-filha-' + idProduto + '.ativo');
	valorPai = document.querySelector('#texto-selected-' + idProduto).textContent;
	medidaValores = valorPai;
	contadorValoresFilhas = valoresFilhas.length;
	for(i = 0; i < contadorValoresFilhas; i++){
		valorFilha = valoresFilhas[i].textContent;
		medidaValores = medidaValores + '|' + valorFilha; 
	}
	console.log(medidaValores);
	jQuery.ajax({
		url: admin_url,
		type: 'POST',
		async: true,
		data: {
			action: 'adicionarProduto',
			referencia: codigoRef, 
			nome: tituloProduto, 
			imagem_url: imagemUrl,
			quantidade: quantidadeProduto,
			titulos_medidas: titulosArray,
			medidas_valores: medidaValores,
			produto_id: idProduto
		},
		success: function(response){
			var quantidade = response.split("|")[0];
			document.getElementById("quantidade-carrinho").innerHTML = quantidade;
			modalSuccessController();
			atualizarCarrinhoLista();
		},
		error: function(err){
			console.log(err);
		},
	});
}

function zerar(admin){
	produtos = [];
	atualizarCarrinho(produtos, admin);
}

function modalSuccessController(){
	document.getElementById('container-response-pai').classList.add('success');
	setTimeout(function(){
		document.getElementById('container-response-pai').classList.remove('success');
	},5000)
}

function modalSuccessFechar(){
	document.getElementById('container-response-pai').classList.remove('success');
}

function enviarViaWhatsapp(numero){
	texto = document.getElementById('txt_itens').textContent;
	texto = texto.replace(/(\r\n|\n|\r)/gm, "%0a");
	window.open('https://api.whatsapp.com/send?phone=' + numero + '&text=' + texto)
}

function atualizarCarrinho(itens, admin_url){
	
	texto = "";
	quantidade = itens.length
	for ( i = 0; i < quantidade; i ++ ) {
		texto += "\n" + itens[i].nome + " (" +itens[i].referencia + ")\nQuantidade: " + itens[i].quantidade + "\n" + JSON.stringify(itens[i].medidas).replace(/{|}|"/g,' ').replace(' , ','\n') + "\n\n"; 
	}

	if(document.getElementById("txt_itens") != null){
		document.getElementById("txt_itens").innerHTML = texto;
	}


	carrinho =  {itens: itens}
	jQuery.ajax({
		url: admin_url,
		type: 'POST',
		async: true,
		data: {
			action: 'atualizarCarrinho', 
			carrinho: JSON.stringify(carrinho) 
		},
		success: function(response){
			var quantidade = response.split("|")[0];
			document.getElementById("quantidade-carrinho").innerHTML = quantidade;
			atualizarCarrinhoLista();
		},
		error: function(err){
			console.log(err);
		},
	});

}

function atualizarCarrinhoLista(){
	jQuery.ajax({
		url: url,
		type: 'POST',
		async: true,
		data: {
			action: 'atualizarCarrinhoLista'
		},
		beforeSend: function() {
			jQuery('.container-carrinho-ajax').empty();
		},
		success: function(response){
			if (!jQuery('#element').is(':empty')){
				jQuery('.container-carrinho-ajax').empty();
			}
			if(response == 'false'){
				var semProdutos = document.createElement("p");
				semProdutos.innerText = "Não há produtos em seu carrinho";
				jQuery('.container-carrinho-ajax').append(semProdutos);
			}else{
				var arrayPosts = JSON.parse(response);
				var countArrayPosts = arrayPosts.length;

				var total = document.createElement("span");
				total.innerText = arrayPosts[0].total + " itens na lista de orçamento";

				var botaoEnviarCarrinho = document.createElement("div");
				botaoEnviarCarrinho.className = 'container-botao-carrinho';

				var linkCarrinho = document.createElement("a");
				linkCarrinho.href = '/orcamento';
				linkCarrinho.innerText = 'Enviar orçamento';

				botaoEnviarCarrinho.appendChild(linkCarrinho);

				jQuery('.container-carrinho-ajax').append(total);
				jQuery('.container-carrinho-ajax').append(botaoEnviarCarrinho);

				for(i = 0; i < countArrayPosts; i++){
					var containerDivPrincipal = document.createElement("div");
					containerDivPrincipal.className = 'container-produto linha-produto-carrinho';
					containerDivPrincipal.id = 'linha-produto-carrinho-' + i;

					var containerDivInfo = document.createElement("div");
					containerDivInfo.className = 'container-info-pai';

					var containerDivTitulo = document.createElement("div");
					containerDivTitulo.className = 'container-titulo';

					var containerDivInfoFilha = document.createElement("div");
					containerDivInfoFilha.className = 'container-info';

					var containerDivQuantidade = document.createElement("div");
					containerDivQuantidade.className = 'container-quantidade-pai';

					var containerDivQuantidadeFilha = document.createElement("div");
					containerDivQuantidadeFilha.className = 'container-quantidade';

					var containerDivQuantidadeProduto = document.createElement("div");
					containerDivQuantidadeProduto.className = 'quantidade-produto';
					
					var containerDivInfoFilha = document.createElement("div");
					containerDivInfoFilha.className = 'container-info';

					var containerDivRemove = document.createElement("div");
					containerDivRemove.className = 'container-remove';

					var containerDivRemoveImagem = document.createElement("div");
					containerDivRemoveImagem.className = 'container-remove-image';
					containerDivRemoveImagem.setAttribute('onclick','cancelarProdutoCarrinho("'+ i +'")');

					var nome = document.createElement("h1");
					nome.innerText = arrayPosts[i].nome;

					var medidas = document.createElement("p");
					medidasText = arrayPosts[i].medidas;
					medidas.innerText = medidasText;

					var quantidadeTitulo = document.createElement("p");
					quantidadeTitulo.innerText = "Qtd";

					var buttonAdicionar = document.createElement("button");
					buttonAdicionar.innerText = "+";
					buttonAdicionar.setAttribute('onclick','aumentarDiminuirQuantidadeProdutoCarrinho("'+ i +'", "+")');

					var buttonDiminuir = document.createElement("button");
					buttonDiminuir.innerText = "-";
					buttonDiminuir.setAttribute('onclick','aumentarDiminuirQuantidadeProdutoCarrinho("'+ i +'", "-")');

					var quantidadeInput = document.createElement("input");
					quantidadeInput.value = arrayPosts[i].quantidade;
					quantidadeInput.className = 'quantidade-produto-input';
					quantidadeInput.id = 'quantidade-produto-carrinho-' + i;
					quantidadeInput.type = 'number';
					quantidadeInput.readOnly = true;

					var imagemLixeira = document.createElement("img");
					imagemLixeira.src = '/wp-content/themes/trezaco/img/lixeira.svg';

					containerDivTitulo.appendChild(nome);
					containerDivTitulo.appendChild(medidas);

					containerDivQuantidadeProduto.appendChild(buttonAdicionar);
					containerDivQuantidadeProduto.appendChild(quantidadeInput);
					containerDivQuantidadeProduto.appendChild(buttonDiminuir);
					containerDivQuantidadeFilha.appendChild(quantidadeTitulo);
					containerDivQuantidadeFilha.appendChild(containerDivQuantidadeProduto);
					containerDivQuantidade.appendChild(containerDivQuantidadeFilha);

					containerDivRemoveImagem.appendChild(imagemLixeira);
					containerDivRemove.appendChild(containerDivRemoveImagem);

					containerDivInfoFilha.appendChild(containerDivQuantidade);
					//containerDivInfoFilha.appendChild(containerDivRemove);
					containerDivInfo.appendChild(containerDivTitulo);
					containerDivInfo.appendChild(containerDivInfoFilha);

					containerDivPrincipal.appendChild(containerDivInfo);
					
					jQuery('.container-carrinho-ajax').append(containerDivPrincipal);
				}
			}
		},
		error: function(err){
			console.log(err);
		},
	});
}

function carregarMais(){
	jQuery.ajax({
		url: url,
		type: 'POST',
		async: true,
		data: {
			action: 'be_ajax_load_more',
			page: page,
		},
		beforeSend: function() {
			jQuery('.carregar-mais-texto').text('Carregando...');
		},
		success: function(response){
			if(response == 'false'){
				jQuery('.carregar-mais-texto').text('Não há mais posts');
			}else{
				var arrayPosts = JSON.parse(response);
				var countArrayPosts = arrayPosts.length;

				for(i = 0; i < countArrayPosts; i++){
					var containerDivPrincipal = document.createElement("div");
					containerDivPrincipal.className = 'container-post';

					var containerDivImagem = document.createElement("div");
					containerDivImagem.className = 'container-imagem';

					var containerDivTag = document.createElement("div");
					containerDivTag.className = 'container-tag';

					var containerDivLer = document.createElement("div");
					containerDivLer.className = 'container-ler-mais';

					var titulo = document.createElement("h1");
					titulo.innerText = arrayPosts[i].titulo;

					var resumo = document.createElement("p");
					resumo.innerText = arrayPosts[i].resumo

					var imagem = document.createElement("img");
					imagem.src = arrayPosts[i].imagem;

					var link = document.createElement("a");
					link.href = '/blog/' + arrayPosts[i].link;
					link.innerText = 'Ler postagem';

					containerDivLer.appendChild(link);
					containerDivImagem.appendChild(imagem);
					containerDivImagem.appendChild(containerDivTag);
					containerDivPrincipal.appendChild(containerDivImagem);
					containerDivPrincipal.appendChild(titulo);
					containerDivPrincipal.appendChild(resumo);
					containerDivPrincipal.appendChild(containerDivLer);

					jQuery('.container-posts-blog').append(containerDivPrincipal);
				}
				jQuery('.carregar-mais-texto').text('Ver mais posts');
				jQuery('.container-corpo-blog').append( button );
				page = page + 1;
			}
		},
		error: function(err){
			jQuery('.carregar-mais-texto').text('Ocorreu algum erro');
		},
	});
}
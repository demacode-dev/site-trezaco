<?php
    wp_enqueue_style('css_blog', get_stylesheet_directory_uri().'/src/css/blog.min.css', array(), null, false);
    get_header();

    $categoria_selecionada = get_query_var('categoriablog');
    $pesquisaBlog = get_query_var('pesquisablog');
    $listaMeses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
?>
<div class="container-breadcrumb">
    <div class="container-padrao">
        <img class="home" src="<?php echo get_stylesheet_directory_uri()?>/img/home-solid.svg">
        <img class="arrow" src="<?php echo get_stylesheet_directory_uri()?>/img/orange-next-arrow.svg">
        <a href="/blog">Blog</a>
    </div>
</div>
<div class="container-posts-blog-pai">
    <div class="container-padrao container-corpo-blog">
        <div class="container-tags">
            <?php 
                $argsBlogTags = array(
                    'post_type'  => 'blog',
                    'taxonomy' => 'categoria_blog',
                    'orderby' => 'name',
                    'order'   => 'ASC',
                    'parent' => '0',
                    'hide_empty' => false
                );  
                $blogTags = get_terms($argsBlogTags);

                if(!empty($blogTags)){
                    $contadorTags = count($blogTags);
                    for($j = 0; $j < $contadorTags; $j++){
                        $tag = $blogTags[$j];
            ?>
                <div class="container-tag anime anime-fade" style="transiton-delay: .<?=$j?>s">
                    <a href="/blog/categoria/<?=$tag->slug?>"> <?=$tag->name?> </a>
                </div>
            <?php } } ?>
        </div>
        <div class="container-posts-blog">
            <?php 
                $tax_query = array();
                if(!empty($categoria_selecionada)) {
                    $tax_query =  array(
                        array(
                            'taxonomy' => 'categoria_blog',
                            'field' => 'slug',
                            'terms' => $categoria_selecionada
                        )
                    );
                }

                $argsBlogPosts = array(
                    'post_type'  => 'blog',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'parent' => '0',
                    'hide_empty' => false,
                    'posts_per_page' => 8,
                    'numberposts' => 8,
                    'tax_query' => $tax_query,
                    's' => $pesquisaBlog
                );  
                $blogPosts = get_posts($argsBlogPosts);
                if(!empty($blogPosts)){
                    $contadorPosts = count($blogPosts);
                    for($j = 0; $j < $contadorPosts; $j++){
                        $postBlog = $blogPosts[$j];
                        if($j < 2){
                            $dataHora = $postBlog->post_date;
                            $dataHoraSplit = explode(' ', $dataHora);
                            $data = explode('-', $dataHoraSplit[0]);
                            $dataAno = $data[0];
                            $dataMes = $data[1]; 
                            $dataDia = $data[2];
            ?>
                <div class="container-post-total anime anime-fade">
                    <div class="container-imagem">
                        <img src="<?=get_field('imagem_destaque', $postBlog->ID)['url']?>">
                    </div>
                    <div class="container-conteudo">
                        <h1>
                            <?php 
                                if(strlen(get_field('titulo', $postBlog->ID)) > 40){
                                    echo substr(get_field('titulo', $postBlog->ID),0, 40)."..."; 
                                }else{
                                    echo get_field('titulo', $postBlog->ID); 	
                                }
                            ?>
                        </h1>
                        <p>
                            <?php 
                                if(strlen(get_field('texto', $postBlog->ID)) > 120){
                                    echo substr(get_field('texto', $postBlog->ID),0, 120)."..."; 
                                }else{
                                    echo get_field('texto', $postBlog->ID); 	
                                }
                            ?>
                        </p>
                        <div class="container-autor">
                            <div class="container-imagem-autor">
                                <img src="<?= !empty(get_field('imagem_do_autor', $postBlog->ID)['url']) ? get_field('imagem_do_autor', $postBlog->ID)['url'] : get_stylesheet_directory_uri().'/img/perfil-desc.jpg'?>">
                            </div>
                            <div class="container-info">
                                <p><?= !empty(get_field('nome_do_autor', $postBlog->ID)) ? get_field('nome_do_autor', $postBlog->ID) : 'Desconhecido'?></p>
                                <p><?=$listaMeses[((int)$dataMes - 1)]?> <?=$dataDia?>, <?=$dataAno?></p>
                            </div>
                        </div>
                        <div class="container-ler-mais">
                            <a href="/blog/<?= $postBlog->post_name?>">Ler postagem</a>
                        </div>
                    </div>
                </div>
            <?php } else{ ?>
                <div class="container-post anime anime-fade">
                    <div class="container-imagem">
                        <img src="<?=get_field('imagem_destaque', $postBlog->ID)['url']?>">
                        <div class="container-tag">
                            <p></p>
                        </div>
                    </div>
                    <h1>
                        <?php 
                            if(strlen(get_field('titulo', $postBlog->ID)) > 40){
                                echo substr(get_field('titulo', $postBlog->ID),0, 40)."..."; 
                            }else{
                                echo get_field('titulo', $postBlog->ID); 	
                            }
                        ?>
                    </h1>
                    <p>
                        <?php 
                            if(strlen(get_field('texto', $postBlog->ID)) > 120){
                                echo substr(get_field('texto', $postBlog->ID),0, 120)."..."; 
                            }else{
                                echo get_field('texto', $postBlog->ID); 	
                            }
                        ?>
                    </p>
                    <div class="container-ler-mais">
                        <a href="/blog/<?= $postBlog->post_name?>">Ler postagem</a>
                    </div>
                </div>
            <?php } } }else{ ?>
                <div class="container-nao-ha">
                    <p>Nenhum post foi encontrado</p>
                </div>
            <?php } ?>
        </div>
        <div class="carregar-mais" onclick="carregarMais()">
            <p class="carregar-mais-texto">Ver mais posts</p>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function($){
    });
</script>
<?php get_footer(); ?>